# Extraction des lignes par transformée de Hough

La transformée de Hough est une technique utilisée en traitement d'image pour détecter des formes géométriques. On cherche ici à implémenter un extracteur de caractéristiques propre à notre contexte : extraire les lignes de contour des parois du circuit correspondant chacune à une composante de couleur. 

## Introduction

La transformée de Hough est basée sur le principe que les points d'une forme géométrique peuvent être représentés par des paramètres mathématiques. Par exemple, une ligne peut être représentée par son équation cartésienne $y = m \times x + b$, où m est la pente et b est l'ordonnée à l'origine.

## Étapes de la transformée de Hough

La transformée de Hough se décompose en plusieurs étapes :

1. **Conversion de l'image** en une représentation appropriée, telle qu'une image en niveaux de gris ou une image binaire.
2. **Définition de l'espace des paramètres**, qui est une représentation discrète des paramètres possibles de la forme géométrique que nous voulons détecter.
3. **Accumulation des votes** dans l'espace des paramètres pour chaque point de l'image qui correspond à la forme géométrique.
4. **Recherche des maximums dans l'espace des paramètres**, qui correspondent aux paramètres les plus probables de la forme géométrique.
5. **Interprétation** des pics pour obtenir les paramètres de la forme géométrique détectée.


<figure markdown="span">
    ![Extraction des lignes](images/hough/line_extraction_process.png){ width="500" }
    <figcaption>Extraction des lignes par transformée de Hough</figcaption>
</figure> 

!!! success "Extraction des lignes"
    Nous constatons que nous arrivons à extraire les lignes de contour des parois du circuit de façàn nominale. Avec un peu de géométrie il est alors possible de déterminer la position de la voiture dans son environnement immédiat en fonction des coefficients directeurs de chaque ligne et de la position du point de fuite.

## Recherche des paramètres optimaux

Pour une détection précise des lignes, une qualité d'image basse suffit. Nous ajustons les paramètres de la transformée de Hough pour s'adapter à la modification de la taille de l'image. Les paramètres à ajuster sont :

- ``threshold`` : Le seuil de détection d'une ligne en fonction de la valeur de l'accumulateur dans l'espace des paramètres.
- ``minLineLength`` : La longueur minimale d'une ligne pour qu'elle soit reconnue.
- ``maxLineGap`` : La taille maximale d'un trou dans une ligne pour la considérer continue.

Les paramètres suivants conviennent :

| Taille de l'image | Seuil | minLineLength | maxLineGap |
| ----------------- | ----- | ------------- | ---------- |
| 512x512           | 80    | 20            | 50         |
| 256x256           | 70    | 10            | 30         |
| 170x170           | 25    | 10            | 20         |
| 128x128           | 25    | 10            | 20         |

=== "Originale"
    <figure markdown="span">
        ![Extraction des lignes](images/hough/image_size_comp_original.png){ width="700" }
        <figcaption>Extraction des ligne sur l'image originale</figcaption>
    </figure> 

=== "512x512"
    <figure markdown="span">
        ![Extraction des lignes](images/hough/image_size_comp_512.png){ width="700" }
        <figcaption>Extraction des ligne sur l'image 512x512</figcaption>
    </figure> 

=== "256x256"
    <figure markdown="span">
        ![Extraction des lignes](images/hough/image_size_comp_256.png){ width="700" }
        <figcaption>Extraction des ligne sur l'image 256x256</figcaption>
    </figure> 

=== "170x170"
    <figure markdown="span">
        ![Extraction des lignes](images/hough/image_size_comp_170.png){ width="700" }
        <figcaption>Extraction des ligne sur l'image 170x170</figcaption>
    </figure> 

=== "128x128"
    <figure markdown="span">
        ![Extraction des lignes](images/hough/image_size_comp_128.png){ width="700" }
        <figcaption>Extraction des ligne sur l'image 128x128</figcaption>
    </figure> 

??? tip "Observations"
    Dans notre contexte la transformée de Hough est plus robuste sur les images basse résolution. En diminuant la résolution de l'image on simplifie ses caractéristiques géométriques ce qui est équivalent à un moyennage dans l'espace des caractéristiques de Hough : on récupère donc plus facilement les coordonnées du point de fuite sur une image basse résolution. 
    > On combine ainsi fiabilité et rapidité


Pour une implémentation en temps réel on cherche à être rapide c'est pourquoi nous choisirons image de taille 128x128.

!!! example "Mesure du temps d'inférence à l'oscilloscope"
    Avec la même technique que précédemment nous mesurons le temps d'exécution de l'algorithme de Hough : la fréquence de fonctionnement mon à près de 17Hz d'après nos mesures.

=== "``threshold``"
    **Variation du seuil de détection d'une ligne** : Nous comparons les résultats pour différentes valeurs du seuil de détection d'une ligne en fixant arbitrairement les autres paramètres.
    <figure markdown="span">
        ![Comparaison pour différentes valeurs de threshold](images/hough/image_threshold_comp.png){ width="400" }
        <figcaption>Comparaison pour différentes valeurs de threshold</figcaption>
    </figure> 

=== "``minLineLen``"
    **Variation de la longueur minimale de la ligne** : Nous comparons les résultats pour différentes valeurs de la longueur minimale d'une ligne en utilisant **threshold = 25** et **maxLineGap = 20**.
    <figure markdown="span">
        ![Comparaison pour différentes valeurs de threshold](images/hough/image_minLen_comp.png){ width="400" }
        <figcaption>Comparaison pour différentes valeurs de threshold</figcaption>
    </figure> 

=== "``maxLineGap``"
    **Variation de la longueur maximale d'un trou** : Nous comparons les résultats pour différentes valeurs de la longueur maximale d'un trou dans une ligne pour la considérer d'un seul trait.
    <figure markdown="span">
        ![Comparaison pour différentes valeurs de maxLineGap](images/hough/image_maxGap_comp.png){ width="400" }
        <figcaption>Comparaison pour différentes valeurs de maxLineGap</figcaption>
    </figure> 

!!! tip "Paramètres optimaux"
    1. La valeur **threshold = 25** est optimale.
    2. La valeur **minLineLen = 10** est optimale.
    3. La valeur **maxLineGap = 20** est optimale.

## Reconstitution de la scène à partir des caractéristiques extraites

!!! note "Construction du point critique"
    1. On calcul le point d'intersection des deux lignes principales d'une composante de couleur donnée
    2. Le point critique est définit comme étant le milieu du segment contruit à partir de ces points d'intersection.
    3. On seuil le résultat pour avoir un point de fuite dans le cadre quelque soit la situation, le nombre de composantes de couleur dans le champ, le nombres de lignes détectées.

<figure markdown="span">
    ![](images/hough/hough_webots_2.png){ width="600" }
</figure> 

<figure markdown="span">
    ![Comparaison pour différentes valeurs de maxLineGap](images/hough/hough_webots_3.png){ width="600" }
</figure> 

??? example "``getVanishingPoint``"

    ``` python title="getVanishingPoint"
    def getVanishingPoint(self, redLines, greenLines) :

        redPoint, greenPoint = True, True
        try :
            rline1 = redLines[0][0]
            rline2 = redLines[1][0]
            r_pt_y = 0.25*sum([rline1[1], rline1[3], rline2[1], rline2[3]])
            r_pt_x = max(rline1[0], rline1[2], rline2[0], rline2[2])
        except IndexError :
            redPoint = False
        
        try :
            gline1 = greenLines[0][0]
            gline2 = greenLines[1][0]
            g_pt_x = min(gline1[0], gline1[2], gline2[0], gline2[2])
            g_pt_y = 0.25*sum([gline1[1], gline1[3], gline2[1], gline2[3]])
        except IndexError :
            greenPoint = False

        criticalPoint = (0, 0)
        if redPoint and greenPoint :
            criticalPoint = (0.5*(r_pt_x + g_pt_x), 0.5*(r_pt_y + g_pt_y))
        else :
            if redPoint :
                criticalPoint = (self.img_width, r_pt_y)
            elif greenPoint :
                criticalPoint = (0, g_pt_y)

        return criticalPoint
    ```

# Conclusion 

## Compresion de l'information et extraction sémantique

!!! warning "Avant le pré-traitement"
    - Image 128x128 px
    - 3 canaux : RGB

    $$
    128 \times 128 \times 3 \times 8 = 393216 \text{ bits}
    $$

!!! success "Après le pré-traitement"
    - 2 histogrammes de 128 valeurs
    - Les coordonnées du point de fuite

    $$
    ( 128 \times 2 + 2 ) \times 8 = 2064 \text{ bits}
    $$

    Soit une compression de près de <span style="color: green; font-size: 200%;"> ***99,5%*** </span>


<figure markdown="span">
    ![Extraction des lignes et histogrammes associés](images/hough/image_hist_lines_2.png){ width="700" }
    <figcaption>Extraction des lignes et histogrammes associés</figcaption>
</figure>


Les techniques de pré-traitement déployées permettent de reconstituer le contexte avec une très bonne fidelité tout en compressant grandement l'information. On a ainsi réussi à extraire l'information sémantique de l'image avant de la transmettre au réseau de neurone, on peut donc espérer entrainer un réseau de neurone directement à partir des informations utiles de la course ce qui permet d'utiliser un réseau de neurone plus léger et plus rapide : c'est-à-dire avec moins de couches et moins de neurones.
