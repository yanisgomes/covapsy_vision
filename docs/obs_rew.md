## Fonction `get_observation`

La fonction `get_observation` est au cœur du processus d'apprentissage par renforcement de notre voiture autonome. Elle est responsable de la collecte des observations provenant de la caméra embarquée sur la voiture.

### Signature de la fonction
