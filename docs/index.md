# TER : RL Vision - Covapsy 2024

Ce tutoriel est un compte rendu détaillé du travail fourni par Yanis Gomes dans le cadre de son TER, travail encadré de recherche, pour le master M1E3A de l'ENS Paris-Saclay.

![Covapsy 2024](images/index/covapsy-2024.png)

## Introduction

Dans le cadre de la ***COVAPSY 2024***, la Course de Voitures Autonomes de Paris-Saclay nous avons développé une voiture autonome capable de naviguer de manière autonome en utilisant comme seule vecteur d'information une caméra embarquée.
Nous utilisons une approche basée sur l'apprentissage par renforcement pour entraîner un réseau de neurones à piloter la voiture. La caméra capture des images de l'environnement traitées en temps réel par la Raspberry Pi qui exécute les inférences du réseau de neurones. L'apprentissage par renforcement permet à la voiture d'apprendre à conduire en expérimentant différentes actions et en observant les résultats. Nous utilisons le simulateur Webots pour simuler un environnement de conduite réaliste et entraîner notre réseau de neurones. Ce tutoriel détaillera le processus de développement de notre voiture autonome, y compris la configuration de la Raspberry Pi, l'installation de Webots, l'entraînement du réseau de neurones et la mise en œuvre du contrôle de la voiture en pratique.

## Contexte et objectif
notre objectif est de démontrer pour la première fois qu'il est possible de faire fonctionner une voiture de manière autonome en utilisant uniquement une caméra embarquée sur le toit. Cette approche diffère des Lidars utilisés par les autres participants, qui sont limités par leur fréquence mécanique de 10 tours par seconde. En utilisant la caméra, nous pouvons théoriquement dépasser cette limite de fréquence et nous souhaitons le démontrer pratiquement. Travailler à une fréquence plus élevée permet une meilleure réactivité de la voiture, ce qui se traduit par une vitesse accrue.

??? note "Quelques ordre de grandeur"

    La vitesse maximale de nos voitures de course est de 25 km/h : 
    
    > Vitesse en m/s = 25 × (1000 / 3600) ≈ 6.94 m/s

    Si nous comparons cela à la fréquence mécanique de 10 Hz d'un Lidar. La voiture avance de : 6.94 m/s × 0.1 s ≈ 70cm Donc si la caméra peut traiter des images à une fréquence supérieure, par exemple 20 Hz alors chaque image est prise tous les : 1 s / 20 ≈ 0.05 s. La distance parcourue par image de caméra = 6.94 m/s × 0.05 s ≈ 35cm.

    Le gain d'anticipation est considérable :

    === "Avec lidar"
        <figure markdown="span">
            ![lidar](images/index/odg_70.png){ width="500" }
            <figcaption>Distance équivalente du temps d'inférence du modèle embarqué aevc un lidar</figcaption>
        </figure>

    === "Avec caméra"
        <figure markdown="span">
            ![caméra](images/index/odg_35.png){ width="500" }
            <figcaption>Distance équivalente du temps d'inférence du modèle embarqué avec une caméra</figcaption>
        </figure>


**L'utilisation d'une caméra à haute fréquence réduit considérablement la distance entre chaque prise de vue permettant ainsi d'améliorer considérablement la réactivité du véhicule autonome. Avantage déterminant pour augmenter la vitesse moyenne de la voiture durant la course tout en assurant une conduite une trajectoire sécurisée et efficace.**


