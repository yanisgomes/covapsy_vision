# Utilisation de la bibliothèque picamera2 pour la Covpasy 2024

La Covpasy 2024 est une voiture autonome équipée d'une caméra et d'une Raspberry Pi. Dans ce tutoriel, nous allons voir comment utiliser la bibliothèque `picamera` pour capturer des images avec la caméra embarquée.

## Installation

Avant de commencer, assurez-vous d'avoir installé la bibliothèque `picamera` sur votre Raspberry Pi. Vous pouvez l'installer en exécutant la commande suivante :
