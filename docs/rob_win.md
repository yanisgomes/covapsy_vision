## Création de la ``robot_window``

!!! question "Problématique"
    ## Comment s'assurer que les données simulés sont correctement pré-traités ?

!!! tip "Solution : créer une interface controller/utlisateur"
    La ``robot_window`` est une solution proposée par Webots pour créer une interface entre un ***Robot Node*** et l'utilisateur. Elle permet de transmettre des données dans les deux sens : du **controller** vers l'utiliateur et de l'utilisateur vers le **controller**, on peut ainsi visualiser ou contrôler les paramètres d'un robot en cours de simulation de façon ergonomique.

<figure markdown="span">
    ![Extraction des lignes et histogrammes associés](images/webots/robot_window.png){ width="700" }
    <figcaption>Principe de la ``robot_window``</figcaption>
</figure>

!!! example "Comment créer une ``robot_window`` ?"
    === "Etape 1 : ``controllerArgs``"
        Spécifier le code **HTML** de la ``robot_window`` dans ``controllerArgs``

        <figure markdown="span">
            ![Spécifier un argument du controller](images/webots/controller_args.png){ width="220" }
            <figcaption>Passer la ``robot_window`` en argument du controller</figcaption>
        </figure>

        !!! warning "Contriante sur le chemin d'accès"
            Le code **HTML** de la ``robot_window`` doit se trouver dans un répertoire donné :
            Dans le dossier ``plugins/robot_windows/`` sachant que le dossier ``plugins`` se trouve au même niveau que le dossier ``controller``

        
    === "Etape 2 : ``wwiSendText``"

        Pour envoyer des données depuis le **controller** on utilise la méthode ``wwiSendText`` de la classe ``Driver`` de Webots.
        On peut dès lors envoyer les paramètres qui caractérisent l'état du robot à chaque pas de la simulation c'est-à-dire à chaque passage dans la méthode ``step``. 
        !!! abstract "Méthode"
            On utilise la méthode ``json.dumps`` pour convertir un dictionnaire en chaîne de caractère et ainsi le passer en argument à la méthode ``wwiSendText``.
            On peut donc facilement organiser les données qui caractérisent l'état courant du robot dans un dictionnaire avec des clefs adaptées.

        ??? tip "Convertir une image en str ?"
            On utilise l'encodage **JPEG** de l'image pour utilier la méthode `b64encode` du package `base64` qui convertit l'image en ``str`` :

            ``` python title="Conversion de l'image en str"
            img_wwi = cv2.cvtColor(img_colorized_128, cv2.COLOR_BGR2RGB)
            retval, buffer = cv2.imencode(".jpeg", img_wwi)
            jpeg_as_text = base64.b64encode(buffer).decode()
            self.stateData['image'] = jpeg_as_text
            ```

        ```python title="Envoie d'une chaîne de caractère depuis le controller"
        	# Step function
            def step(self, action) :

                """Perform a step in the environment
                Here ...
                """
                    
                self.set_vitesse_m_s(self.consigne_vitesse)
                self.set_direction_degre(self.consigne_angle)
                super().step()

                if self.robotWindowEnabled :
                    self.wwiSendText(json.dumps(self.stateData))
                
                obs = self.get_observation()
                reward, done = self.get_reward(obs)
                truncated = False
                info = {}

                return obs, reward, done, truncated, info
        ```

    === "Etape 3 : Paramétrer le navigateur"

        1. **Choisir un navigateur **par défaut de la ``robot_window`` :
        Tools > Preferences > Default robot window web browser

        2. **Désactiver le cache du navigateur** choisi pour s'assurer que le navigateur recharge les nouveaux scripts HTML/JS/CSS à chaque fois que l'on relance une nouvelle ``robot_window``. Cette étape est cruciale pour voir les modifications apportées à la robot_window en continu lors de son implémentation.

        3. **Implémentation en HTML/JS** dans le même script pour éviter les erreurs de mise en cache même si l'étape 2 est réalisée correctement il arrive selon les navigateurs que les politiques de mise en cache JS et HTML diffèrent. On implémente donc la partie JS directement au sein du code HTMl entre les balises : ``<scritp type="module">`` et ``</script>``

            a. On importe l'objet ``RobotWIndow`` depuis le site de Webots : 
            ``` python title="Import du module RobotWindow.js"
            import RobotWindow from 'https://cyberbotics.com/wwi/R2023b/RobotWindow.js'
            ```

            b. On implémente la fonction qui réceptionne les données envoyés via ``wwiSendText`` du script Python :
            ``` javascript title="Fonction receive"
            function receive(message) {

                const data = JSON.parse(message);

                conste img_str = data.image;

                const imageElement = document.getElementById('robotImage');

                if (imageElement) {
                    imageElement.src = 'data:image/jpeg;base64,' + data.image;
                }
            } 
            ```

            c. On definit la fonction receive comment étant celle qui traite les messages reçu par la fenêtre courante :
            ``` javascript title="Fonction receive"
            window.onload = function() {
                window.robotWindow = new RobotWindow();
                window.robotWindow.receive = receive;
            };
            ```
    
    === "Etape 4 : Lancer la ``robot_window``"
        On lance la ``robot_window`` à partir d'un click droit sur le robot en question dans la simulation : _Show robot window_ 

        <figure markdown="span">
            ![Spécifier un argument du controller](images/webots/right_click.png){ width="400" }
            <figcaption>Clique droit sur le robot pour ouvrir sa robot_window</figcaption>
        </figure>


## Résultats obtenus en simulation

<figure markdown="span">
    ![Extraction des lignes et histogrammes associés](images/webots/robot_window_1.png){ width="700" }
</figure>

<figure markdown="span">
    ![Extraction des lignes et histogrammes associés](images/webots/rw2.png){ width="700" }
</figure>

<figure markdown="span">
    ![Extraction des lignes et histogrammes associés](images/webots/rw3.png){ width="700" }
</figure>

<figure markdown="span">
    ![Extraction des lignes et histogrammes associés](images/webots/rw4.png){ width="700" }
</figure>

<figure markdown="span">
    ![Extraction des lignes et histogrammes associés](images/webots/rw5.png){ width="700" }
</figure>

<figure markdown="span">
    ![Extraction des lignes et histogrammes associés](images/webots/rw6.png){ width="700" }
</figure>

### Ouverture

La robot_window peut contenir tout sorte d'interaction avec l'utilisateur il est donc posible de modifier certains pramètres en cours de simulation cela eput s'avérer être utile pour certains réglages fins de la voiture. Par ailleurs on a décidé ici de créer une robot_window pour la voiture que l'on entraine mias il est tout à fait possible de créer une robot_window pour le ***superviseur***. Ainsi on pourrait centraliser les informations de l'entrainement en cours, afficher certaines métriques mesurées depuis le script du superviseur, voire modifier son comportement en cours de simulation. De façon générale la ``robot_window`` permet de créer une IHM ergonomique avec l'environnement simulé et l'utilisateur.

??? example "Le code de la ``robot_window`` au complet"
    === "HTML &JS"
        ```HTML
        <!DOCTYPE html>
        <html>
        <head>
        <meta charset='UTF-8'>
        <link rel='stylesheet' type='text/css' href='robot_window_vision2.css'>
        <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
        </head>
        <body>
            <div id="title">
            <h1>RL Vision - RW</h1>
        </div>
        <div id="picam">
            <p id="camTitle">Line identification through color extraction</p>  
            <img id="robotImage" src="" alt="Launch simulation to get the embedded camera analysis here">
        </div>
        <div id="data">
            <h4 id="speed">Waiting for last speed data</h4>
            <h4 id="angle">Waiting for last angle data</h4>
        </div>
        <div id="histContainer">
            <h4 id="histTitle">En attente des données de l'histogramme...</h4>
            <canvas id="histCanvas"></canvas>
        </div>
        <script type="module">
            import RobotWindow from 'https://cyberbotics.com/wwi/R2023b/RobotWindow.js'              
            var histChart = null;
            var proportionChart = null;
            var latestRewards = Array(10).fill(0);
            function updateRawardBarChart(barChartId, rewardsDict) {
            const ctx = document.getElementById(barChartId).getContext('2d');
            var rewards = Object.values(rewardsDict);
            var labels = Object.keys(rewardsDict);
            if (!window.rewardBarChart) {
                window.rewardBarChart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        label: labels,
                        datasets: [{
                            label: 'Rewards',
                            data: rewards,
                            backgroundColor: 'rgba(75, 192, 192, 0.2)',
                            borderColor: 'rgba(75, 192, 192, 1)',
                            borderWidth: 1
                        }]
                    },
                    options: {
                        scales: {
                            y: {
                                min: -400,
                                max: 100,
                                beginAtZero: false // L'axe des Y s'ajuste automatiquement
                            },
                            x: {
                                ticks: {
                                    maxTicksLimit: 50 // Limite le nombre de labels sur l'axe des X pour éviter l'encombrement
                                }
                            }
                        },
                        plugins: {
                            legend: {
                                labels: {
                                    color: '#ffffff',
                                    font: {
                                        size: 16, 
                                        family: 'Arial, sans-serif'
                                    }
                                }
                            }
                        }
                    }
                });
            } else {
                // Met à jour les labels et les données pour refléter l'état actuel de data.rewards
                window.rewardBarChart.data.label = labels;
                window.rewardBarChart.data.datasets[0].data = rewards;
                window.rewardBarChart.update();
            }
            }
            function updateRewardHistogram(histId, rewards) {
            const ctx = document.getElementById(histId).getContext('2d');
            var rewardsList = rewards; // Obtient les 10 dernières récompenses
            const rewardsLength = rewardsList.length;
            if (!window.rewardChart) {
                window.rewardChart = new Chart(ctx, {
                    type: 'line', // Utilisation d'un graphique linéaire
                    data: {
                        labels: Array.from({length: rewardsLength}, (_, i) => i + 1), // Crée un tableau de labels basé sur la longueur de data.rewards
                        datasets: [{
                            label: 'R = ' + rewardsList[rewardsLength - 1],
                            data: rewardsList,
                            fill: 'origin',
                            borderColor: 'rgb(75, 192, 192)',
                            backgroundColor: 'rgba(75, 192, 192, 0.2)',
                            tension: 0.4 // Rend la ligne plus lisse
                        }]
                    },
                    options: {
                        scales: {
                            y: {
                                min: -400,
                                max: 100,
                                beginAtZero: false // L'axe des Y s'ajuste automatiquement
                            },
                            x: {
                                ticks: {
                                    maxTicksLimit: 50 // Limite le nombre de labels sur l'axe des X pour éviter l'encombrement
                                }
                            }
                        },
                        plugins: {
                            legend: {
                                labels: {
                                    color: '#ffffff',
                                    font: {
                                        size: 16, 
                                        family: 'Arial, sans-serif'
                                    }
                                }
                            }
                        }
                    }
                });
            } else {
                // Met à jour les labels et les données pour refléter l'état actuel de data.rewards
                window.rewardChart.data.labels = Array.from({length: rewardsLength}, (_, i) => i + 1);
                window.rewardChart.data.datasets[0].label = 'R = ' + rewardsList[rewardsLength - 1];
                window.rewardChart.data.datasets[0].data = rewards;
                window.rewardChart.data.datasets[0].backgroundColor = 'rgba(75, 192, 192, 0.2)';
                window.rewardChart.update();
            }
            }
            function drawHistogram(histId, redHistData, greenHistData, redPrct, greenPrct) {     
            const canvasElement = document.getElementById(histId);
            if (!canvasElement) {
                console.error(`Canva element with ID '${histId}' not found.`);
                return;
            } 
            const ctx = canvasElement.getContext('2d');           
            if (!histChart) {
                histChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: redHistData.map((_, index) => index.toString()),
                    datasets: [{
                    label: `Red : ${(redPrct).toFixed(1)}%`,
                    data: redHistData,
                    backgroundColor: 'rgba(255, 99, 132, 0.2)',
                    borderColor: 'rgba(255, 99, 132, 1)',
                    borderWidth: 1
                    },
                    {
                    label: `Green : ${(greenPrct).toFixed(1)}%`,
                    data: greenHistData,
                    backgroundColor: 'rgba(99, 255, 132, 0.2)',
                    borderColor: 'rgba(99, 255, 132, 1)',
                    borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                    y: {
                        beginAtZero: true
                    }
                    },
                    plugins: {
                            legend: {
                                labels: {
                                    color: '#ffffff',
                                    font: {
                                        size: 16, 
                                        family: 'Arial, sans-serif'
                                    }
                                }
                            }
                        }
                },
                plugins: {
                    annotation: {
                    annotations: {
                        line1: {
                        type: 'line',
                        mode: 'vertical',
                        scaleID: 'x-axis-0',
                        value: '59', // x-coordinate for the line
                        borderColor: 'skyblue',
                        borderWidth: 2,
                        borderDash: [10,5], // Create dashed line
                        borderDashOffset: 5
                        },
                        line2: {
                        type: 'line',
                        mode: 'vertical',
                        scaleID: 'x-axis-0',
                        value: '69', // x-coordinate for the line
                        borderColor: 'skyblue',
                        borderWidth: 2,
                        borderDash: [10,5], // Create dashed line
                        borderDashOffset: 5
                        }
                    }
                    }
                }
                });
            } else {
                histChart.data.datasets[0].label = `Red : ${(redPrct).toFixed(1)}%`;
                histChart.data.datasets[1].label = `Green : ${(greenPrct).toFixed(1)}%`;
                histChart.data.datasets[0].data = redHistData;
                histChart.data.datasets[1].data = greenHistData;
                histChart.update();
                }
            }                   
            function receive(message) {                   
            const data = JSON.parse(message);
            const imageTitleElement = document.getElementById('camTitle');
            if (imageTitleElement) {
                imageTitleElement.textContent = data.state;
            }                  
            const imageElement = document.getElementById('robotImage');
            if (imageElement) {
                imageElement.src = 'data:image/jpeg;base64,' + data.image;
            }                 
            const speedElement = document.getElementById('speed');
            if (speedElement) {
                speedElement.textContent = `V = ${data.speed}m.s-1`;
            }                  
            const angleElement = document.getElementById('angle');
            if (angleElement) {
                angleElement.textContent = `A = ${data.angle}°`;
            }                  
            const histElementTitle = document.getElementById('histTitle');
            if (histElementTitle) {
                histElementTitle.textContent = "Histogrammes des composantes de l'image";
            }
            // Draw the histogram
            drawHistogram('histCanvas', data.redHist, data.greenHist, data.redProportion*100, data.greenProportion*100);
            // Update the reward histogram
            updateRewardHistogram('rewardHistogramCanvas', data.rewardsMemory);
            updateRawardBarChart('rewardBarChart', data.rewardsDict);
            }                
            window.onload = function() {
            window.robotWindow = new RobotWindow();
            window.robotWindow.receive = receive;
            };
        </script>
        </body>
        </html>
        ```

    === "CSS"
        ``` CSS
        body {
        margin: 0;
        height: 100vh;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        background-color: #2B2B2B; /* Example background color */
        color: #F6F6F6; /* Example text color */
        font-family: Arial, sans-serif; /* Example font */
        }

        h1, h2 {
        margin: 0; /* Remove default margin */
        }

        #title, #picam {
        text-align: center;
        margin-bottom: 40px; /* Space between sections */
        }

        #robotImage {
        min-width: 80%;
        max-width: 90%;
        height: auto;
        margin-top: 10px; /* Space above the image */
        }

        #rewardHistogramContainer, #histContainer {
        height: 300px; /* Ajustez selon vos besoins */
        }
        canvas {
        width: 80% !important;
        height: 80% !important;
        }
        ```

