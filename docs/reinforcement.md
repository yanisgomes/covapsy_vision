# Apprentissage par Renforcement

## Cours de Master sur l'Apprentissage par Renforcement

L'apprentissage par renforcement est une méthode d'apprentissage machine dans laquelle un agent apprend à effectuer des actions dans un environnement afin de maximiser une certaine mesure de récompense cumulative. Ce domaine s'appuie sur l'évaluation des interactions de l'agent avec son environnement et l'adaptation de ses stratégies en fonction des retours (récompenses) reçus.

### Concepts de Base

#### Environnement

L'**environnement** représente le cadre dans lequel l'agent opère. Il est défini par un ensemble d'états et la dynamique qui régit les transitions entre ces états en réponse aux actions de l'agent.

#### Agent

L'**agent** désigne l'entité qui agit dans l'environnement. Il peut observer l'état de l'environnement, prendre des décisions (actions), et recevoir des récompenses.

#### États

Un **état** est une configuration particulière de l'environnement à un moment donné. L'ensemble des états possibles forme ce qu'on appelle l'espace des états.

#### Actions

Une **action** est une intervention effectuée par l'agent, qui peut modifier l'état de l'environnement. L'ensemble des actions disponibles forme ce qu'on appelle l'espace des actions.

#### Récompenses

Une **récompense** est un feedback immédiat donné à l'agent en réponse à une action effectuée dans un certain état. L'objectif de l'agent est de maximiser la somme des récompenses qu'il reçoit au cours du temps.

### Politique

Une **politique** (notée généralement π) est une stratégie utilisée par l'agent pour choisir ses actions. Formellement, c'est une distribution de probabilité sur les actions conditionnée par les états de l'environnement. La politique peut être déterministe, où une action spécifique est choisie pour chaque état, ou stochastique, où une action est choisie selon une distribution de probabilité dépendant de l'état.

### Revenu et Facteur d'Amortissement

Le **revenu** est la somme cumulative des récompenses que l'agent s'attend à accumuler, souvent pondérée par un **facteur d'amortissement** γ (0 ≤ γ ≤ 1). Ce facteur réduit l'importance des récompenses futures, ce qui peut être crucial dans des environnements où les récompenses immédiates prévalent sur les gains à long terme.

$$
R_t = \sum_{k=0}^{\infty} \gamma^k r_{t+k+1}
$$

### Fonction Valeur de l'État et Fonction Valeur de l'Action

- La **fonction valeur de l'état** V(s) est une estimation du revenu attendu lorsque l'agent commence dans l'état s et suit une politique $\pi :

$$
V^\pi(s) = \mathbb{E} \left[ R_t \mid s_t = s \right]
$$

- La **fonction valeur de l'action** Q(s, a) estime le revenu attendu à partir de la prise de l'action a dans l'état s, puis en suivant la politique π :

$$
Q^\pi(s, a) = \mathbb{E} \left[ R_t \mid s_t = s, a_t = a \right]
$$

### Processus Décisionnel de Markov (MDP)

Un **processus décisionnel de Markov** est un cadre mathématique utilisé pour formuler des problèmes d'apprentissage par renforcement. Il est caractérisé par un ensemble d'états (S), un ensemble d'actions (A), une fonction de transition état-action (P), et une fonction de récompense (R).

### Équations de Bellman

Les équations de Bellman sont fondamentales en apprentissage par renforcement, car elles fournissent une manière récursive de calculer les valeurs des états et des actions.

#### Équation de Bellman pour la fonction valeur de l'état :

$$
V^\pi(s) = \sum_{a \in A} \pi(a \mid s) \sum_{s' \in S} P(s' \mid s, a) \left[ R(s, a, s') + \gamma V^\pi(s') \right]
$$

#### Équation de Bellman pour la fonction valeur de l'action :

L'équation de Bellman pour la fonction valeur de l'action, aussi connue sous le nom de Q-valeur, se définit comme suit :

$$ Q^\pi(s, a) = \sum_{s' \in S} P(s' \mid s, a) \left[ R(s, a, s') + \gamma \sum_{a' \in A} \pi(a' \mid s') Q^\pi(s', a') \right] $$

Cette équation indique que la valeur de prendre une action a dans un état s sous une politique π est le gain immédiat \( R(s, a, s') \) plus la valeur attendue des états futurs, amortie par le facteur γ. L'espérance ici est calculée en prenant en compte toutes les actions futures possibles selon la politique π.

### Méthodes d'Apprentissage par Renforcement

Après avoir exploré les principes généraux et les équations fondamentales de l'apprentissage par renforcement, nous abordons ici trois méthodes clés : SARSA, Q-learning et les réseaux de neurones profonds Q (Deep Q-Networks, DQN), qui sont des techniques d'apprentissage par renforcement basées sur l'apprentissage temporel (Temporal Difference, TD).

#### SARSA (State-Action-Reward-State-Action)

SARSA est une méthode d'apprentissage par renforcement qui utilise la mise à jour on-policy pour apprendre la fonction valeur de l'action directement à partir de l'expérience acquise. La mise à jour se fait selon la formule suivante :

$$ Q(s, a) \leftarrow Q(s, a) + \alpha \left[r + \gamma Q(s', a') - Q(s, a)\right] $$

où:
- \( (s, a, r, s', a') \) est la séquence de l'état, l'action, la récompense, l'état suivant et l'action suivante.
- \( \alpha \) est le taux d'apprentissage.
- \( \gamma \) est le facteur d'amortissement.

La caractéristique principale de SARSA est qu'elle suit la politique actuelle pour choisir \( a' \) (l'action suivante), ce qui en fait une méthode on-policy.

#### Q-learning

Contrairement à SARSA, Q-learning est une méthode off-policy qui cherche à apprendre la valeur optimale de Q indépendamment de la politique suivie par l'agent. La mise à jour de Q-learning se fait selon la formule :

$$ Q(s, a) \leftarrow Q(s, a) + \alpha \left[r + \gamma \max_{a'} Q(s', a') - Q(s, a)\right] $$

Dans Q-learning, l'agent choisit l'action \( a' \) qui maximise la valeur de Q dans l'état suivant \( s' \), sans nécessairement suivre la politique définie par la valeur de Q. Cette méthode tend à converger vers la politique optimale plus rapidement en théorie, parce qu'elle évalue directement la meilleure action possible à chaque étape.

#### Deep Q-Networks (DQN)

Les DQN utilisent des réseaux de neurones profonds pour approximer la fonction valeur de l'action Q. Cette approche est particulièrement utile dans des environnements avec un grand nombre d'états, où le stockage de la table des valeurs de Q devient impraticable. Les DQN modifient l'approche traditionnelle du Q-learning en utilisant un réseau de neurones pour prédire toutes les valeurs de Q à partir d'un état donné.

La mise à jour de la fonction valeur dans DQN se fait en ajustant les poids du réseau de neurones pour minimiser la différence entre la valeur prédite par le réseau et la valeur cible, calculée comme suit :

$$ y = r + \gamma \max_{a'} Q(s', a'; \theta^-) $$

où \( \theta^- \) sont les paramètres du réseau de neurones utilisé pour évaluer l'action à l'état suivant (ce réseau est souvent appelé le réseau cible et est mis à jour périodiquement avec les poids du réseau principal pour améliorer la stabilité de l'apprentissage).

### Résumé Mathématique de l'Entraînement d'un Réseau de Neurones par Reinforcement Learning

Lors de l'entraînement d'un réseau de neurones pour l'apprentissage par renforcement, le but est de trouver les paramètres du réseau (poids) qui minimisent la différence entre les valeurs Q prédites et les valeurs Q cibles. Ceci est généralement accompli via un processus itératif où:

1. Une action est choisie selon une politique basée sur les valeurs Q actuelles (par exemple, ε-greedy).
2. L'agent exécute l'action, observe la récompense et le nouvel état.
3. La valeur cible est calculée en utilisant la récompense observée et la valeur Q prédite pour le nouvel état.
4. Les poids du réseau sont ajustés pour minimiser l'erreur entre la valeur Q prédite et la valeur cible, généralement en utilisant des méthodes de descente de gradient.

Ce cycle se répète pour de nombreux épisodes et pas de temps, permettant au réseau

### Conclusion

L'apprentissage par renforcement est un domaine riche et complexe de l'intelligence artificielle qui enseigne aux agents comment prendre des décisions optimales en explorant et en exploitant activement leur environnement. Les fonctions valeur de l'état et de l'action, ainsi que les équations de Bellman, sont au cœur de nombreuses stratégies d'apprentissage par renforcement, offrant un cadre puissant pour la compréhension et l'optimisation des comportements des agents.

Les défis principaux incluent la grande dimensionnalité des espaces d'états et d'actions, l'incertitude de la mesure et de la perception, ainsi que la nécessité d'un équilibre entre l'exploration de nouvelles actions et l'exploitation des connaissances acquises. L'avancée des recherches et des applications pratiques dans ce domaine continue d'améliorer notre capacité à développer des systèmes autonomes capables d'apprendre et de s'adapter de manière autonome à une multitude de tâches complexes.
