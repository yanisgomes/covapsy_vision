# Prétraitement de l'image

On se propose dans un premier temps de trouver un pré-traitement de l'image qui nous permette de tirer profit de l'***a priori*** que l'on sait sur les images qui vont être traitées par le réseau de neurone. Sachant que le circuit comporte certaines contraintes : 

- Le mur de gauche est <span style="color:red">rouge</span>
- Le mur de droite est <span style="color:green">vert</span>

Il est donc possible de créer concevoir un extracteur de caractéristique en amont du réseau de neurone qui soit un bon compromis entre analyse sémantique de l'image et complexité.
>  ou comment extraire un maximum d'information pertinente en un minimum de calcul.

On se donnera pour exemple le pré-traitment d'une image ayant les mêmes ***a priori*** que les images prisent par la caméra embarqué : *Une course de voiture à la première personne dont les parois gauches et droites du cricuit sont respectivement vertes et rouges.*

<figure markdown="span">
![Exemple d'image pour illustrer le pré-traitment](images/histograms/covapsy.png){ width="500" }
<figcaption>Image de la covapsy 2124 générée par DALL-E 3</figcaption>
</figure>


# Robustesse au bruit

On cherche dans un premier temps à être robuste au bruit, le signal que l'on traite ici correspond aux données brut de la caméra et on peut distinguer des bruits de différentes natures :

1. **Bruit de mesure** introduit par la sous-résolution de la caméra
2. **Bruit sémantique** introduit par la présence d'objets dans le champ de la caméra n'ayant pas de sens pour la conduite de la voiture : trace sur le mur, reflet sur le sol, t-shirt de couleur à l'arrière plan, etc.


## Optimisation des paramètres de pré-traitement

!!! question "Quel niveau de flou gaussien ? Quel seuil d'extraction des couleurs ?"
    On cherche à évaluer l'influence des différents niveaux de flou gaussien et de seuillage binaire sur l'extraction des composantes de couleur d'une image.

On effectue des tests pour différentes valeurs de taille de noyau et de seuil pour les composantes rouge et verte séparément :

1. On applique un flou gaussien pour différentes tailles de noyaux dont on cherche un bon compromis.
2. On effectue un seuillage binaire à différents niveaux de seuil pour extraire la couleur avec plus ou moins d'exactitude.

!!! example "Fonction de seuillage binaire"
    La fonction ``threshold_color_extraction`` convertit l'image RGB en HSV, crée des masques pour isoler les composantes rouges et vertes, puis applique un seuillage binaire pour convertir les images des composantes en un masque binaire.

    ``` python title="threshold_color_extraction"
    def threshold_color_extraction(img_rgb, threshold) :
        img_hsv = cv2.cvtColor(img_rgb, cv2.COLOR_RGB2HSV)

        # RED MASK
        lower_red1 = np.array([0, 120, 70])
        upper_red1 = np.array([10, 255, 255])
        lower_red2 = np.array([160, 120, 70])
        upper_red2 = np.array([180, 255, 255])
        mask1 = cv2.inRange(img_hsv, lower_red1, upper_red1)
        mask2 = cv2.inRange(img_hsv, lower_red2, upper_red2)
        red_mask = mask1 + mask2

        # GREEN MASK
        lower_green = np.array([40, 70 , 70])  # Plus sombre / peu sature
        upper_green = np.array([70, 255, 255])  # Plus clair / tres sature
        green_mask = cv2.inRange(img_hsv, lower_green, upper_green)

        # Red component extraction
        red_only = cv2.bitwise_and(img_rgb, img_rgb, mask=red_mask)
        red_only_rgb = cv2.cvtColor(red_only, cv2.COLOR_HSV2RGB)
        red_only_bin = cv2.cvtColor(red_only, cv2.COLOR_RGB2GRAY)
        ret, red_only_bin = cv2.threshold(red_only_bin, threshold, 255, cv2.THRESH_BINARY)

        # Green component extraction
        green_only = cv2.bitwise_and(img_rgb, img_rgb, mask=green_mask)
        green_only_rgb = cv2.cvtColor(green_only, cv2.COLOR_HSV2RGB)
        green_only_bin = cv2.cvtColor(green_only, cv2.COLOR_RGB2GRAY)
        ret, green_only_bin = cv2.threshold(green_only_bin, threshold, 255, cv2.THRESH_BINARY)

        return red_only_bin, green_only_bin
    ```

## Filtre passe-bas gaussien

On se propose d'essayer un filtre passe-bas gaussien pour différentes valeurs d'écart-type $\sigma$. Cela revient à considérer différentes tailles de noyau d'après la formule $k = 2 \lceil 3\sigma \rceil + 1$. On obtient ainsi une complexité en $\mathscr{O}(N^2K^2)$.

- **Taille du noyau** $k \in \{11, 31, 51, 71, 91\}$
- **Seuil binaire** $z \in \{0, 40, 80, 120\}$

=== "Composante verte"
    <figure markdown="span">
      ![Extraction de la composante verte avec filtre gaussien](images/histograms/GREEN_EXTRACT_gaussian_blur.png){ width="500" }
      <figcaption>Seuillage binaire avec flou gaussien sur la composante verte</figcaption>
    </figure>

=== "Composante rouge"
    <figure markdown="span">
      ![Extraction de la composante rouge avec filtre gaussien](images/histograms/RED_EXTRACT_gaussian_blur.png){ width="500" }
      <figcaption>Seuillage binaire avec flou gaussien sur la composante rouge</figcaption>
    </figure>

## Filtre passe-bas moyenneur

On se propose d'essayer un filtre passe-bas moyenneur car sa complexité de calcul est moindre comparé à celle du filtre gaussien puisqu'elle ne dépend pas de la taille du noyau, elle est en $\mathscr{O}(N^2)$ 

- **Taille du noyau** $k \in \{11, 31, 51, 71, 91\}$
- **Seuil binaire** $z \in \{0, 40, 80, 120\}$

=== "Composante verte"
    <figure markdown="span">
      ![Extraction de la composante verte avec filtre moyenneur](images/histograms/GREEN_EXTRACT_mean_blur.png){ width="500" }
      <figcaption>Seuillage binaire avec filtre moyenneur sur la composante verte</figcaption>
    </figure>

=== "Composante rouge"
    <figure markdown="span">
      ![Extraction de la composante rouge avec filtre moyenneur](images/histograms/RED_EXTRACT_mean_blur.png){ width="500" }
      <figcaption>Seuillage binaire avec filtre moyenneur sur la composante rouge</figcaption>
    </figure>

!!! tip "Importance du filtre passe-bas"
    Les figures précédentes illustrent bien l'impotance d'utiliser un filtre passe-bas ayant une fréquence de coupure spatiale faible, c'est-à-dire un grand noyau, pour ne conserver que les caractéristiques spatiales majeures de l'image.

## Taille de noyau et seuil retenus pour une complexité optimale

1. ***A propos du noyau*** : un noyau de taille élevée permet une meilleure distinction des caractéristiques majeures de l'image. Pour minimiser la complexité de calcul, on utilisera donc un filtre moyenneur et une taille de noyau élevée. Une valeur impaire entre 50 et 100 convient.
2. ***A propos du seuil binaire*** : il est clair que cela dépend de la composante de couleur qui nous intéresse puisque le vert est plus représenté que le rouge en moyenne. Pour la composante rouge, un seuil de 40 convient. Pour la composante verte, un seuil supérieur à 100 convient.

!!! success "Valeurs retenues"

    - **Composante rouge :** $(k_r^*, z_r^*) = (51, 40)$
    - **Composante verte :** $(k_g^*, z_g^*) = (71, 120)$

    On retiendra deux couples de paramètres pour les composantes de couleur qui nous intéressent, les grandeurs optimales seront notées par une étoile :

    $$ k^* = 71, z_r^* = 40, z_g^* = 120 $$ 

## Calcul des histogrammes horizontaux et verticaux

!!! abstract "A propos de la notion d'histogramme"
    En traitement de l'image le mot histogramme correspond à un objet qui dénombre la quantité de pixel par dynamique dans la globalité de l'image. Ici le mot ***histogramme*** est un abus de langage car il correspond au dénombrement de pixels par colonne ou par ligne pour un masque binaire ce qui revient à sommer les valeurs des pixels par colonne ou par ligne : par histogramme on entend donc la projection sur l'axe des abscisses ou des ordonnées de l'image filtrée par un opérateur d'extraction d'une composante de couleur, ici rouge ou vert. 

    ### Formalisation mathématique
    Soit $\mathcal{I}$ l'image de taille $N \times M$ décrite par sa dynamique $I$ telle que :

    $$
    \forall (x, y) \in \left[ 0, N-1 \right] \times \left[ 0, M-1 \right], I(x, y) \in \left[ 0, 255 \right] ^ 3
    $$

    On définit $\Gamma_R$ et $\Gamma_G$ les fonctions de masquage binaire permettant d'extraire espectivement les couleurs rouge et vertes. Soient $\mathcal{C}_R$ et $\mathcal{C}_G$ les domaines associés aux couleurs à extraires. On note par $K$ la choisir choisie parmie $\{R, G\}$ on a donc :

    $$
    \Gamma_K(I(x, y)) = \begin{cases} 1 & \text{si } I(x, y) \in \mathcal{C}_K \\ 0 & \text{sinon } \end{cases}
    $$

    Par la on nommera histogramme l'opérateur de projection normalisée sur un des axes de l'images :

    - **$\mathcal{H}_K$ histogramme horizontale de la composante $K$** :

    $$
    \forall i \in \left[ 0, N-1 \right],  \mathcal{H}_K(i) = \frac{1}{M} \sum_{y = 0}^{M-1} \Gamma_K (I(i, y))
    $$

    - **$\mathcal{V}_K$ histogramme verticale de la composante $K$** :

    $$
    \forall j \in \left[ 0, M-1 \right],  \mathcal{V}_K(j) = \frac{1}{N} \sum_{y = 0}^{N-1} \Gamma_K (I(i, y))
    $$


=== "Image originale"
    <figure markdown="span">
      ![Image originale](images/histograms/covapsy.png){ width="500" }
      <figcaption>Image originale de la covapsy 2124</figcaption>
    </figure>

=== "Histogrammes de l'image originale"
    <figure markdown="span">
      ![Histogrammes horizontaux et verticaux sur l'image originale](images/histograms/color_extraction_hist_original.png){ width="500" }
      <figcaption>Histogrammes horizontaux et verticaux sur l'image originale</figcaption>
    </figure>

=== "Histogrammes de l'image filtrée"
    <figure markdown="span">
      ![Histogrammes horizontaux et verticaux sur l'image filtrée](images/histograms/color_extraction_hist_blurred_k71.png){ width="500" }
      <figcaption>Histogrammes horizontaux et verticaux sur l'image filtrée</figcaption>
    </figure> 

## Inférence de l'extraction des histogrammes de couleur sur la RaspberryPi

En supprimant la partie de l'affichage qui concerne l'affichage de la figure Matplotlib on mesure la fréquence pour différentes tailles d'image NxN :

!!! tip "Mesure de la fréquence"
    On utilise les fonctions de base de la RaspberryPi pour complémenter l'état d'une broche GPIO à chaque itération de la boucle du calcul d'histogramme. On mesure ensuite à l'oscilloscope la fréquence du signal en créneaux ainsi obtenu. En négligeant le temps de complémentation de la broche GPIO on en déduit donc l'inférence de l'algorithme d'extraction des histogramems des composantes de couleur comme étant égale au double de la fréquence mesurée à l'oscilloscope.

- **N = 64** : $f = 17Hz$
- **N = 128** : $f = 16.6Hz$ : Bon compromis
- **N = 800** : $f = 10Hz$

## Conclusion

Au terme de cette ce premier prétraitement par histogrammes nous avons mis en lumière l'efficacité d'une stratégie combinant différentes techniques de filtrage et de seuillage pour extraire de manière précise les composantes de couleur rouge et verte correspondant aux murs du circuit. L'application d'un filtre gaussien, suivie d'un seuillage binaire optimisé, s'est révélée être un équilibre judicieux pour l'extraction des données sémantiques de l'image dans le contexte de la **covpasy**.

Notre approche permet non seulement de réduire l'impact du bruit mais aussi de conserver les caractéristiques spatiales importantes de l'image. Ceci est crucial pour l'entraînement du réseau de neurones en apprentissage par renforcement, où la capacité à identifier de manière fiable et rapide les murs du circuit peut significativement influencer la performance globale du système autonome.

Les mesures de fréquence effectuées sur le dispositif de calcul indiquent que l'algorithme est tout à fait déployable pour notre application. On obtient finalement un bon compromis entre extraction sémantique et vitesse d'exécution. Cela nous permet d'envisager sereinement l'intégration de ce prétraitement dans notre pipeline de traitement d'image en temps réel, garantissant ainsi que notre véhicule autonome puisse réagir efficacement aux dynamiques du circuit.

Maintenant que nous avons démontré l'intérêt du pré-traitement par histogramme nous allons raffiner ce pré-traitement en implémentant un extracterud e ligne à l'aide de la transformée de Hough.



