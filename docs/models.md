# Les différents modèles entrainés

## Modalités de récompense

Pour l'entrainement des modèles on test différentes fonctions de récompenses pour obtenir le meilleur pilotage possible.
Pour ce faire on distingue différentes modalités de récompense ou de pénalités : 

1. **Pénalité en fonction de la proximité aux murs**
2. **Alignement avec la piste**
3. **Encouragement de la vitesse**
4. **Compensation de la dynamique en virage**
5. **Respect d'une plage de vitesse donnée**

### **Pénalité en fonction de la proximité aux murs**
On cherche à pénaliser le déséquilibre de couleur en considérant que c'est un marqueur de non alignement de la trajectoire avec la piste.
On évalue cette pénalité $\mathcal{P}_1$ comme une fonction polynomiale de la différence en valeur absolue des proportions des composantes de couleur dans l'image courante :

$$
\mathcal{R}_1 = - A \times \frac{1}{N} \Big{|} \sum_{i = 0}^{M-1} \mathcal{H}_R(i) - \mathcal{H}_G(i) \Big{|}^\alpha
$$

### **Alignement avec la piste**
On cherche à pénaliser le manque d'alignement entre la trajectoire de la voiture et le point de fuite obtenu par la transformée de Hough. On considère pour cela la valeur absolu de l'abscisse du point critique centré, c'est-à-dire à zéro quad il se trouve au milieu de l'image. On évalue cette pénalité $\mathcal{P}_1$ comme une fonction polynomiale de cette valeur absolue :

$$
\mathcal{R}_2 = - B \times \Big{|} \text{self.criticalPointAbs} \Big{|}^\beta
$$

### **Encouragement de la vitesse**
On cherche à encourager la vitesse de la voiture à un instant donnée indépendamment du contexte :

$$
\mathcal{R}_3 = - C \times \Big{|} \text{self.speed} \Big{|}^\gamma
$$

## **Alignement et dynamique en virage**

On se propose de raffiner le modèle pour anticiper le fait que le déséquilibre des proportions des couleurs et le désalignement du point de fuite ne devraient pas pénaliser l'agent quand il est entrain de tourner. Si on note par $\theta$ la valeur du de l'angle de braquage on considère la péanlité suivante :

$$
\mathcal{R}_4 = - D \times \Big{|} \theta \Big{|} \times \Big{|} \text{self.speed} \Big{|}^\delta
$$

## **Respect d'une plage de vitesse donnée**

On se propose d'utiliser une fonction de récompense de la vitesse polynomiale par morceaux. On définit une plage de vitesse nominale, et des valeurs asymptotiques de pour manque ou excès de vitesse. On relie le tout par des fonctions polynomiales. On obtient ainsi un profil de récompense simple à caractériser avec des paramètres qualitatifs à adapter en fonction des autres pénalités. On obtient différents profils de récompense associé à une telle fonction :

??? example "Implémentation de la fonction de récompense polynomiale "
    ```python title="Fonction de récompense de la vitesse"
    def speedRewardFunction(self, speed_m_s) :
		"""Non-linear reward function based on the speed"""

		V = speed_m_s * 3.6 # Convert speed to km/h
		Vmax = MAXSPEED     # Maximum speed in km/h

		Vnom_min = NOMSPEED_MIN # Minimum nominal speed in km/h
		Vnom_max = NOMSPEED_MAX   # Maximum nominal speed in km/h

		Rmin = -1000    # Minimum reward when V <= Vnom_min
		Rmin2 = -600   # Minimum reward when V >=  Vnom_max
		Rmax = 500     # Maximum reward when Vnom_min < V <= Vnom_max

		if V <= Vnom_min:
			# Growth power 4 for V <= Vnom
			R = Rmin + (Rmax - Rmin) * (V / Vnom_min) ** 3
		elif V <= Vnom_max:
			# Constant for Vnom_min < V <= Vnom_max
			R = Rmax
		else :
			# Quadratic decrease for V > Vnom
			R = Rmin2 + ((Vmax - V) / (Vmax - Vnom_max)) ** 5 * (Rmax - Rmin2)

		return R
	```

=== "Profil 1"
    <figure markdown="span">
        ![Profil 1](images/models/reward_1.png){ width="400" }
    </figure>

=== "Profil 2"
    <figure markdown="span">
        ![Profil 2](images/models/reward_2.png){ width="400" }
    </figure>

=== "Profil 3"
    <figure markdown="span">
        ![Profil 3](images/models/reward_3.png){ width="400" }
    </figure>


## Modèle Evolution

**Fonction de récompense** = $\mathcal{R}_1$

!!! success "Remarques"
    Yo



## Modèle Revolution

**Fonction de récompense** = $\mathcal{R}_1 + \mathcal{R}_2$

!!! abstract "Remarques"
    Yo



## Modèle Meta-evolution

**Fonction de récompense** = $\mathcal{R}_1 + \mathcal{R}_2 + \mathcal{R}_3$

!!! warning "Remarques"
    Yo



## Modèle Armageddon

**Fonction de récompense** = $\mathcal{R}_1 + \mathcal{R}_2 + \mathcal{R}_3 + \mathcal{R}_4$

!!! danger "Remarques"
    Yo



## Modèle Phoenix

**Fonction de récompense** = $\mathcal{R}_1 + \mathcal{R}_5$

!!! success "Remarques"
    Yo




