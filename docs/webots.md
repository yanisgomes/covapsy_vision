# Simulation et environnement d'apprentissage

Le simulateur Webots est un outil puissant pour le développement et la simulation de systèmes robotiques. Dans le cadre de notre projet nous avons utilisé Webots pour simuler les conditions de la course et déployer des algorithmes de ***reinforcement learning*** pour entrainer le réseau de neurone qui pilote la voiture.

!!! question "Problématique"
    ## Comment implémenter un environnement d'apprentissage avec ``stablebaselines3``

On se propose d'implémenter une classe permettant d'instancier un environnement d'apprentissage à partir du **framework** de ``stable_baselines3``
avec les données fournies par le monde **Webots**.

## Constructeur de la classe `WebotsGymEnvironment`

La classe `WebotsGymEnvironment` est dérivée de deux superclasses importantes :

- **`Driver`**: permet d'interagir avec les différents dispositifs de contrôle du robot dans l'environnement Webots. Cette intégration facilite la récupération des données sensorielles et l'envoi des commandes de contrôle aux actuateurs du robot.
- **`gym.Env`**: fournit un cadre standardisé pour l'implémentation d'environnements d'apprentissage par renforcement avec `stable_baselines3`, permettant de personnaliser facilement la méthode `step` pour l'exécution de l'algorithme PPO.

??? abstract "Implémentation du contruscteur de la classe `WebotsGymEnvironment`"
    #### Configuration initiale

    Lors de l'initialisation, plusieurs configurations sont effectuées pour préparer l'environnement du robot :

    ```python
    self.robotWindowEnabled = False  # Désactive par défaut la fenêtre robotique

    # Configuration des paramètres initiaux de contrôle
    self.consigne_angle = 0.0  # Angle initial en degrés
    self.consigne_vitesse = VITESSE_MIN_M_S  # Vitesse initiale en mètres par seconde

    # Configuration de la caméra
    self.picam = super().getDevice("camera")
    self.picam.enable(int(super().getBasicTimeStep()))
    self.img_width = 128
    self.img_height = 128
    ```

    #### Dispositifs de communication et de mesure

    ```python
    # Emetteur et récepteur pour la communication
    self.emitter = super().getDevice("emitter")
    self.receiver = super().getDevice("receiver")
    self.receiver.enable(RECEIVER_SAMPLING_PERIOD)

    # Accéléromètre pour la mesure de la vitesse instantanée
    self.accelerometer = self.getDevice("accelerometer")
    self.accelerometer.enable(int(super().getBasicTimeStep()))
    self.instantSpeed = np.array([0.0, 0.0, 0.0])
    ```

    #### Espaces d'actions et d'observations

    La configuration des espaces d'actions et d'observations est cruciale pour définir comment l'agent perçoit son environnement et quelles actions il peut prendre :

    ```python
    # Espaces d'actions: valeurs continues pour l'angle et la vitesse
    self.action_space = gym.spaces.Box(low=np.array([-1.0, -1.0]), high=np.array([1.0, 1.0]), dtype=np.float32)

    # Espaces d'observations: diverses mesures sensorielles et états du robot
    self.observation_space = gym.spaces.Dict({
        "redHist": gym.spaces.Box(low=np.zeros(self.img_width), high=np.ones(self.img_width), dtype=np.float64),
        "greenHist": gym.spaces.Box(low=np.zeros(self.img_width), high=np.ones(self.img_width), dtype=np.float64),
        # Plusieurs autres capteurs et indicateurs
    })
    ```

## Méthodes de pré-traitement de l'image

Les fonctions de traitement d'image vues précédemment sont utilisées dans la méthode ``get_observation`` qui est utilisée automatiquement par ***gym*** lors de l'exécution de la boucle **`PPO`**. 

??? abstract "Méthode de pré-traitement"

    === "Implémentation"
        #### Récupération de l'image de la caméra simulée
        ```python
        def getRGBImage(self):
            # Récupération et traitement de l'image capturée par la caméra
            try:
                img = self.picam.getImage()
                return np.frombuffer(img, np.uint8).reshape((self.picam.getHeight(), self.picam.getWidth(), 4))
            except ValueError:
                print("Erreur d'acquisition d'image")
                return None
        
        #### Extraction des couleurs et des lignes
        On utilise les masques de couleur définis dans le constructeur
        ```python title="Extraction des couleurs"
        def extractColor(self, img_rgb, color : Literal['R', 'G']) :
            """Extracts a color from an RGB image and returns a binary image with only the color in white"""
            img_hsv = cv2.cvtColor(img_rgb, cv2.COLOR_RGB2HSV)
            threshold = 1
            
            if color == 'R' :
                mask1 = cv2.inRange(img_hsv, self.lower_red1, self.upper_red1)
                mask2 = cv2.inRange(img_hsv, self.lower_red2, self.upper_red2)
                color_mask = mask1 + mask2
            elif color =='G' :
                color_mask = cv2.inRange(img_hsv, self.lower_green, self.upper_green)

            color_only = cv2.bitwise_and(img_rgb, img_rgb, mask=color_mask)
            color_only_rgb = cv2.cvtColor(color_only, cv2.COLOR_HSV2RGB)
            color_only_bin = cv2.cvtColor(color_only, cv2.COLOR_RGB2GRAY)
            ret, color_only_bin = cv2.threshold(color_only_bin, threshold, 255, cv2.THRESH_BINARY)

            return color_only_bin
        ```
        On utilise un filtre passe-haut pour appliquer la transformée de Hough avec les paramètres établis dans la première partie.
        ```python title="Extraction des couleurs"
        def extractEdges(self, img_rgb, color : Literal['R', 'G']) :
            """Extracts edges from an RGB image and returns a binary of the Canny filtered image"""
            color_bin_img = self.extractColor(img_rgb, color)
            color_edges = cv2.Canny(color_bin_img, 100, 200)
            return color_edges
        
        def extractLines(self, img_rgb, color : Literal['R', 'G'], threshold=80, minLineLen=10, maxLineGap=50) :
            """Extracts lines from an RGB image and returns a binary of the Hough transformed image"""
            img_edges = self.extractEdges(img_rgb, color)
            imgLines = cv2.HoughLinesP(img_edges, 1, np.pi / 180, threshold=threshold, minLineLength=minLineLen, maxLineGap=maxLineGap)
            return imgLines
        ```

        #### Construction de l'extracteur de caractéristique ``extractLines_adaptativThreshold``
        ```python title="Extraction des couleurs"
        def extractLines_adaptativThreshold(self, img_rgb, color : Literal['R', 'G'], num_lines) :
            """Extracts lines from an RGB image with an adaptativ threshold and returns the main lines list wth the number of iteration of the threshold adaptation"""
            minLineLen = 25
            maxLineGap = 25

            threshold_step = 3
            init_threshold = 25
            threshold = init_threshold + threshold_step
            
            img_edges = self.extractEdges(img_rgb, color)
            imgLines = list()

            # On abaisse le seuil de détection d'une ligne tant qu'on n'en détecte pas suffisamment
            n = 0
            while len(imgLines) < num_lines :
                n += 1
                threshold -= threshold_step
                imgLines = cv2.HoughLinesP(img_edges, 1, np.pi/180, threshold=threshold, minLineLength=minLineLen, maxLineGap=maxLineGap)
                if threshold <= 0 :
                    break
                if imgLines is None :
                    imgLines = list()

            if imgLines is None :
                imgLines = list()

            mainLines = imgLines[:num_lines]
            return (mainLines, n)
        ```
    === "Exécution et affichage"

        L'exécution des méthodes de pré-traitement permettent d'actualiser les attributs de l'agent traités ensuite dans la méthode ``get_observation`` qui est au coeur du processus d'entrainement de la boucle PPO.
        
        On actualise
            ``` python title="Exécution du pré-traitement"
            def embeddedCameraAnalysis(self) :
                """Process the embedded camera data"""
                img_rgb = self.getRGBImage()
                img_rgb_128 = cv2.resize(img_rgb, (self.img_width, self.img_height), interpolation=cv2.INTER_AREA)
                img_bgr_128 = cv2.cvtColor(img_rgb_128, cv2.COLOR_BGR2RGB)

                # Create masks for red and green colors
                red_bin_mask = self.extractColor(img_bgr_128, 'R')
                green_bin_mask = self.extractColor(img_bgr_128, 'G')

                # Lines extraction
                num_lines = 2
                redLines, _ = self.extractLines_adaptativThreshold(img_bgr_128, 'R', num_lines)
                greenLines, _ = self.extractLines_adaptativThreshold(img_bgr_128, 'G', num_lines)

                # Algebric absciss of the vanishing point
                vanishingPoint = self.getVanishingPoint(redLines, greenLines) # ([0, 128], [0, 128])
                vanishingPointAbs = vanishingPoint[0]

                # Vertcial red pixels histogram
                redHist = np.sum(red_bin_mask, axis=0)/255
                # Vertcial green pixels histogram
                greenHist = np.sum(green_bin_mask, axis=0)/255

                if self.robotWindowEnabled :
                    # Irreversible gray transformation
                    img_gray_128 = cv2.cvtColor(img_bgr_128, cv2.COLOR_BGR2GRAY)
                    img_colorized_128 = cv2.cvtColor(img_gray_128, cv2.COLOR_GRAY2BGR)

                    # Use the masks to apply red and green colors to the grayscale image
                    img_colorized_128[red_bin_mask > 0, 0] = img_bgr_128[red_bin_mask > 0, 0]
                    img_colorized_128[green_bin_mask > 0, 1] = img_bgr_128[green_bin_mask > 0, 1]

                    # Plot red and green lines
                    if redLines is not None :
                        for line in redLines:
                            x1, y1, x2, y2 = line[0]
                            cv2.line(img_colorized_128, (x1, y1), (x2, y2), (255, 0, 0), 2)
                    if greenLines is not None :
                        for line in greenLines:
                            x1, y1, x2, y2 = line[0]
                            cv2.line(img_colorized_128, (x1, y1), (x2, y2), (0, 255, 0), 2)

                    # Plot the vanishing point in sky blue
                    cv2.circle(img_colorized_128, (int(vanishingPoint[0]), int(vanishingPoint[1])), 5, (255, 255, 0), -1)

                    img_wwi = cv2.cvtColor(img_colorized_128, cv2.COLOR_BGR2RGB)
                    retval, buffer = cv2.imencode('.jpeg', img_wwi)
                    jpeg_as_text = base64.b64encode(buffer).decode()
                    self.stateData['image'] = jpeg_as_text

                return redHist, greenHist, vanishingPointAbs
            ```

## Fonction d'observation

C'est dans cette méthode centrale que l'on actualise les attributs de la claqse qui caractérise à un instant donné l'état de l'agent dans l'environnement.

??? abstract "Implémentation de la fonction d'observation"
    ``` python title="Actualisation de l'état de l'agent"
    def get_observation(self, init=False) :
        """Get the observation based on camera inputs"""
        current_redHist, current_greenHist, criticalPointAbs = self.embeddedCameraAnalysis()
        last_redHist = self.observation["redHist"]
        last_greenHist = self.observation["greenHist"]
        last_criticalPointAbs = self.observation["criticalPointAbs"][0]

        # Initialize histograms for observation
        redHist_norm = current_redHist / 128
        greenHist_norm = current_greenHist / 128
        last_redHist_norm = last_redHist / 128
        last_greenHist_norm = last_greenHist / 128

        if init :
            # Initialize critical point abscissa
            criticalPointAbs_norm = np.zeros(1)       # criticalPointAbs at center in [-1, 1]
            last_criticalPointAbs_norm =  np.zeros(1) # last_criticalPointAbs at center in [-1, 1]
            # Initialize speed and angle
            last_speed = np.zeros(1)                  # last_speed in [0, 1]
            last_angle = np.zeros(1) 			      # last_angle in [-1, 1]
        else :
            # Normalized critical point abscissa
            criticalPointAbs_norm = np.array([2*criticalPointAbs / 128 - 1]) # ([0, 128] -> [-1, 1])
            last_criticalPointAbs_norm = np.array([last_criticalPointAbs]) # last_criticalPointAbs in [-1, 1]
            # Normalized speed and angle
            last_speed = np.array([super().getCurrentSpeed() / VITESSE_MAX_M_S])
            last_angle = np.array([self.consigne_angle / MAXANGLE_DEGRE])

        observation = {
            "redHist" : redHist_norm,
            "greenHist" : greenHist_norm,
            "last_redHist" : last_redHist_norm,
            "last_greenHist" : last_greenHist_norm,
            "criticalPointAbs" : criticalPointAbs_norm,
            "last_criticalPointAbs" : last_criticalPointAbs_norm,
            "last_speed" : last_speed,
            "last_angle" : last_angle
        }

        self.stateData['speed'] = round(self.observation["last_speed"][0], 1)
        self.stateData['angle'] = round(self.observation["last_angle"][0], 1)
        self.stateData['redHist'] = redHist_norm.tolist()
        self.stateData['greenHist'] = greenHist_norm.tolist()
        self.stateData['redProportion'] = np.sum(redHist_norm) / 128
        self.stateData['greenProportion'] = np.sum(greenHist_norm) / 128

        # DYNAMIC MEASUREMENT
        # Absolute value of the normalized values
        self.speedAbsDerivative = abs(self.observation['last_speed'] - last_speed)
        self.steeringAbsDerivative = abs(self.observation['last_angle'] - last_angle)

        self.observation = observation
        return observation	
    ```

## Fonction de récompense

La définition de la fonction de récompense est déterminante dans l'apprentissage de l'agent : c'est elle qui est au coeur de la boucle PPO puisqu'elle évalue l'état de l'agent suite aux observations que l'on fait de son état. C'est sur cette fonction qui est sujette à la rétro-propagation du gradient et c'est elle que l'on va chercher à affiner au cours des différents modèles déeloppés par la suite.

??? abstract "Implémentation de la fonction récompense"
    ``` python title="Fonction de récompense"
    def get_reward(self, obs) :
        """Fonction de récompense basée sur les entrées de la caméra."""

		# COLOR IMBALANCE PENALTY
		# Color proportion in [0, 1]
		redProp = sum(obs['redHist'])/128   
		greenProp = sum(obs['greenHist'])/128    
		colorImbalanceFactor = 700
		colorImbalancePenalty = -(abs(redProp - greenProp)*colorImbalanceFactor)**1.3
		self.rewardsDict['colorImbalancePenalty'] = colorImbalancePenalty

		# SPEED REWARD
		current_speed = VITESSE_MAX_M_S * obs['last_speed'][0]  # Assumant que la vitesse est normalisée entre 0 et 1
		self.rewardsDict['speedReward'] = self.speedRewardFunction(current_speed)

		done = self.crashed()

		if done :
			self.crashCounter += 1
			self.rewardsDict['crashPenalty'] = -10000
		else :
			self.rewardsDict['crashPenalty'] = 0

		# Condition pour réinitialiser l'environnement après un certain nombre de pas, si nécessaire
		self.noResetCounter += 1
		if self.noResetCounter % RESET_STEP == 0 :
			done = True

		reward = 0.0
		for key in self.rewardsDict.keys() :
			if np.isscalar(self.rewardsDict[key]):
				reward += float(self.rewardsDict[key])
				print('{} = {}'.format(key, float(self.rewardsDict[key])))
			else:
				reward += float(self.rewardsDict[key][0])
				print('{} = {}'.format(key, float(self.rewardsDict[key][0])))
			
		reward = np.round(reward, 1)

		if self.robotWindowEnabled :
			self.stateData['rewardsMemory'].append(reward)
			if len(self.stateData['rewardsMemory']) > self.rewardsMemory :
				self.stateData['rewardsMemory'].pop(0)
			self.stateData['rewardsDict'] = self.stateData['rewardsDict']

		return reward, done
	
    ```

## Condition d'arrêt d'une simulation et méthode ``reset``

La condition d'arrêt est le fruit d'un compromis :

1. Identifier l'état de crash de l'agent
2. Ne pas détecter un crash quand la voiture dans un état transitoire critique

Si la fonction de récompense n'est pas exécutée une fois le modèle entrainé ; la détection de crash, elle, doit se limiter strictement à l'utilisation des données qui sont accessibles depuis le modèle embarqué. En effet cela permet d'améliorer le déploiement du modèle et de le coupler avec un algorithme déterministe qui saura sortir l'agent, ici la voiture, de son état de crash, par exemple en reculant.

??? abstract "Implémentation de la fonction ``reset``"
    ``` python title="Fonction reset"
    def reset(self, seed=None) :
		"""Reset the car position and return an observation."""

		# Reset speed and steering angle
		self.consigne_vitesse = 0
		self.consigne_angle = 0
		self.set_vitesse_m_s(self.consigne_vitesse)
		self.set_direction_degre(self.consigne_angle)

		for i in range(20) :
			super().step()	

		self.noResetCounter = 0
		
		if(self.crashCounter != 0) :         
			# Wait for the car to stop
			while abs(super().getCurrentSpeed()) >= 0.001 :    		           	
				print("Waiting TOTO for the car to stop...")
				self.set_vitesse_m_s(0)
				self.set_direction_degre(0)
				super().setCruisingSpeed(0)
				super().step()
	
			# Return an observation
			self.packet_number += 1
			# Send a message to the supervisor to reset the car
			self.emitter.send("CRASH n°{}".format(str(self.packet_number)))
			super().step()

			# Wait for the supervisor to reset the car
			while(self.receiver.getQueueLength() == 0) :        	
				self.set_vitesse_m_s(self.consigne_vitesse)
				super().step()

			data = self.receiver.getString()
			self.receiver.nextPacket()
			#print(data)
				
			self.consigne_vitesse = 0
			self.consigne_angle = 0
			self.set_vitesse_m_s(self.consigne_vitesse )
			self.set_direction_degre(self.consigne_angle)
			#on fait quelques pas à l'arrêt pour stabiliser la voiture si besoin
			while abs(super().getTargetCruisingSpeed()) >= 0.001 :    		           	
				#print("voiture pas arrêtée")
				self.set_vitesse_m_s(0)
				super().step()
				
		info = {}        
		return self.get_observation(True), info
    ```

### Rôle central de la méthode `step` dans `WebotsGymEnvironment`

La méthode `step` joue un rôle fondamental dans le cadre de l'intégration de l'environnement simulé Webots avec des techniques d'apprentissage par renforcement. Elle est essentielle, car elle permet d'appliquer et de simuler les décisions prises par l'algorithme d'apprentissage, impactant directement l'état de l'environnement et donc le comportement du robot.

??? abstract "Implémentation de la Méthode `step`"
    - **Extraction des Actions :** Les actions reçues en entrée sont décomposées en composantes de vitesse et d'angle, chacune affectée par un facteur d'incrément spécifique pour moduler l'impact de chaque action.
    - **Application des Contraintes :** Les valeurs de vitesse et d'angle sont contrôlées pour rester dans des limites prédéfinies, évitant ainsi des comportements non physiques ou dangereux.
    - **Mise à Jour de Webots :** Les nouvelles valeurs de vitesse et d'angle sont envoyées au simulateur Webots, qui avance d'un pas de simulation.
    - **Collecte d'Observations et de Récompenses :** Après la mise à jour, l'état actuel est observé, une récompense est calculée, et il est vérifié si l'épisode doit se terminer.

    ``` python title="Méthode step"
    # Step function
	def step(self, action) :
		"""Perform a step in the environment."""
		current_speed = self.consigne_vitesse
		current_angle = self.consigne_angle

		# Extract actions from the dictionary
		speed_action = action[0]  # Assuming action space is defined with shape (1,)
		angle_action = action[1]  # Assuming action space is defined with shape (1,)

		incremental_speed_factor = 0.05
		incremental_angle_factor = 9.0
		
		self.consigne_vitesse = (current_speed + speed_action*incremental_speed_factor)
		self.consigne_angle = (current_angle + angle_action*incremental_angle_factor)
		
		# saturations
		if self.consigne_angle > MAXANGLE_DEGRE  :
			self.consigne_angle = MAXANGLE_DEGRE 
		elif self.consigne_angle < -MAXANGLE_DEGRE  :
			self.consigne_angle = -MAXANGLE_DEGRE 
		
		if self.consigne_vitesse > VITESSE_MAX_M_S :
			self.consigne_vitesse = VITESSE_MAX_M_S
		if self.consigne_vitesse < VITESSE_MIN_M_S :
			self.consigne_vitesse = VITESSE_MIN_M_S
			
		self.set_vitesse_m_s(self.consigne_vitesse)
		self.set_direction_degre(self.consigne_angle)
		super().step()

		if self.robotWindowEnabled :
			self.wwiSendText(json.dumps(self.stateData))
		
		obs = self.get_observation()
		reward, done = self.get_reward(obs)
		truncated = False
		info = {}

		return obs, reward, done, truncated, info
    ```

Cette méthode crée donc un lien direct entre les décisions algorithmiques et leur manifestation dans un environnement dynamique, permettant ainsi un apprentissage interactif et adaptatif basé sur la performance et les réactions de l'environnement.


### Le code du modèle Evolution dans son ensemble

??? example "Code du ``controller`` du modèle ***Evolution***"
    ``` python title="Controller du RM Vision Evolution"
    import gymnasium as gym
    import numpy as np
    import time
    from stable_baselines3 import PPO
    from stable_baselines3.common.env_checker import check_env

    import sys
    import cv2
    import json
    import base64
    from typing import Literal

    #sous Linux, chemin a adapter export WEBOTS_HOME=/usr/local/webots
    #sys.path.append('/usr/local/webots/lib/controller/python/')

    #sous windows, chemin a adapter
    sys.path.append('C:/Program Files/Webots/lib/controller/python/')

    # Webots imports
    from vehicle import Driver

    # Global constants
    RECEIVER_SAMPLING_PERIOD = 64	# En milliseconds
    PI = 3.141592653589793
    #TODO : remettre 10 en MAXSPEED
    MAXSPEED = 25 #km/h  
    MINSPEED = 2 #km/h
    NOMSPEED_MIN = 8 #km/h
    NOMSPEED_MAX = 16 #km/h
    VITESSE_MAX_M_S = MAXSPEED/3.6
    VITESSE_MIN_M_S = MINSPEED/3.6
    VITESSE_NOM_MN_M_S = NOMSPEED_MIN/3.6
    VITESSE_NOM_MX_M_S = NOMSPEED_MAX/3.6
    MAXANGLE_DEGRE = 18
    RESET_STEP = 16384	# Number of steps between 2 forced resets to avoid overfitting

    # Custom Gym environment
    class WebotsGymEnvironment(Driver, gym.Env):

        def __init__(self):
            super().__init__()

            self.robotWindowEnabled = False
                    
            #valeur initiale des actions
            self.consigne_angle = 0.0 #en degres
            self.consigne_vitesse =  VITESSE_MIN_M_S #en m/s

            # PICAMERA
            self.picam = super().getDevice("camera")
            self.picam.enable(int(super().getBasicTimeStep()))
            self.img_width = 128
            self.img_height = 128

            # SUPERVISOR SETTINGS
            self.crashCounter = 0 		# compteur de collisions
            self.noResetCounter = 0 	# compteur de pas d'apprentissage pour arrêter un épisode après RESET_STEP pas
            self.packet_number = 0 		# compteur de messages envoyés

            # EMITTER AND RECEIVER
            self.emitter = super().getDevice("emitter")
            self.receiver = super().getDevice("receiver")
            self.receiver.enable(RECEIVER_SAMPLING_PERIOD)

            # ACCELEROMETER
            self.accelerometer = self.getDevice("accelerometer")
            self.accelerometer.enable(int(super().getBasicTimeStep()))
            self.instantSpeed = np.array([0.0, 0.0, 0.0])

            # DYNAMIC PARAMETER 
            # Absolute value of the derivative of the steering angle
            self.steeringAbsDerivative = 0
            self.speedAbsDerivative = 0

            # REWARDS
            self.rewardsDict = dict()

            # ACTION SPACE
            self.action_space = gym.spaces.Box(low=np.array([-1.0, -1.0]), high=np.array([1.0, 1.0]), dtype=np.float32)

            # OBSERVATION SPACE
            self.observation_space = gym.spaces.Dict({
                "redHist": gym.spaces.Box(low=np.zeros(self.img_width), high=np.ones(self.img_width), dtype=np.float64),
                "greenHist": gym.spaces.Box(low=np.zeros(self.img_width), high=np.ones(self.img_width), dtype=np.float64),
                "last_redHist": gym.spaces.Box(low=np.zeros(self.img_width), high=np.ones(self.img_width), dtype=np.float64),
                "last_greenHist": gym.spaces.Box(low=np.zeros(self.img_width), high=np.ones(self.img_width), dtype=np.float64),
                "criticalPointAbs": gym.spaces.Box(low=-1.0, high=1.0, shape=(1,), dtype=np.float64),  # Shape is (1,) for a single scalar value
                "last_criticalPointAbs": gym.spaces.Box(low=-1.0, high=1.0, shape=(1,), dtype=np.float64),
                "last_speed": gym.spaces.Box(low=-1.0, high=1.0, shape=(1,), dtype=np.float64),  # Speed is normalized between -1 and 1
                "last_angle": gym.spaces.Box(low=-1.0, high=1.0, shape=(1,), dtype=np.float64)  # Angle normalized between -1 and 1
            })

            self.observation = {
                "redHist": np.zeros(self.img_width),
                "greenHist": np.zeros(self.img_width),
                "last_redHist": np.zeros(self.img_width),
                "last_greenHist": np.zeros(self.img_width),
                "criticalPointAbs": np.zeros(1),
                "last_criticalPointAbs": np.zeros(1),
                "last_speed": np.zeros(1),
                "last_angle": np.zeros(1)
            }
            
            # RED MASK
            self.lower_red1 = np.array([0, 20, 20])
            self.upper_red1 = np.array([10, 255, 255])

            # self.lower_red2 = np.array([150, 40, 40]) INITIAL ==> crash on webot
            self.lower_red2 = np.array([150, 20, 10]) # Red parameter to adapapt = S, V lower_red2
            self.upper_red2 = np.array([180, 255, 255])

            # GREEN MASK
            self.lower_green = np.array([30, 55 , 55])
            self.upper_green = np.array([85, 255, 255])  

            self.criticalPointAbs = 0
            self.vanishingDepth = 0.5*self.img_height

            # COMPONENT DEPTH HISTOGRAMS
            self.redHist = np.zeros(self.img_width)
            self.greenHist = np.zeros(self.img_width)

            self.redProportion = np.sum(self.redHist) / 128
            self.greenProportion = np.sum(self.greenHist) / 128

            self.rewardsMemory = 40

            # CRASH SETTINGS
            self.crashTimestepCounter = 0
            self.criticalCrashTimestep = 5
            self.MONOCHROME_CRASH_PRCT_MAX = 65
            self.MONOCHROME_CRASH_PRCT_MIN = 0.9

            self.stateData = {
                'image' : "",
                'rewardsDict' : dict(),
                'rewardsMemory' : list(),
                'angle' : self.consigne_angle,
                'speed' : self.consigne_vitesse,
                'redHist' : self.redHist.tolist(),
                'greenHist' : self.greenHist.tolist(),
                'redProportion' : self.redProportion,
                'greenProportion' : self.greenProportion
            }

        def getInstantSpeed(self) :
            """Get the instant speed of the car"""
            acceleration = np.array(self.accelerometer.getValues())
            dt = self.getBasicTimeStep() / 1000
            self.instantSpeed += acceleration * dt
            return np.linalg.norm(self.instantSpeed)
        
        def getRGBImage(self):
            img_rgb = 0
            try :
                img = self.picam.getImage()
                img_rgb = np.frombuffer(img, np.uint8).reshape((self.picam.getHeight(), self.picam.getWidth(), 4))
            except ValueError :
                print("souci d'aquisition image")
            return img_rgb
        
        def extractColor(self, img_rgb, color : Literal['R', 'G']) :
            """Extracts a color from an RGB image and returns a binary image with only the color in white"""
            img_hsv = cv2.cvtColor(img_rgb, cv2.COLOR_RGB2HSV)
            threshold = 1
            
            if color == 'R' :
                mask1 = cv2.inRange(img_hsv, self.lower_red1, self.upper_red1)
                mask2 = cv2.inRange(img_hsv, self.lower_red2, self.upper_red2)
                color_mask = mask1 + mask2
            elif color =='G' :
                color_mask = cv2.inRange(img_hsv, self.lower_green, self.upper_green)

            color_only = cv2.bitwise_and(img_rgb, img_rgb, mask=color_mask)
            color_only_rgb = cv2.cvtColor(color_only, cv2.COLOR_HSV2RGB)
            color_only_bin = cv2.cvtColor(color_only, cv2.COLOR_RGB2GRAY)
            ret, color_only_bin = cv2.threshold(color_only_bin, threshold, 255, cv2.THRESH_BINARY)

            return color_only_bin
        
        def extractEdges(self, img_rgb, color : Literal['R', 'G']) :
            """Extracts edges from an RGB image and returns a binary of the Canny filtered image"""
            color_bin_img = self.extractColor(img_rgb, color)
            color_edges = cv2.Canny(color_bin_img, 100, 200)
            return color_edges
        
        def extractLines(self, img_rgb, color : Literal['R', 'G'], threshold=80, minLineLen=10, maxLineGap=50) :
            """Extracts lines from an RGB image and returns a binary of the Hough transformed image"""
            img_edges = self.extractEdges(img_rgb, color)
            imgLines = cv2.HoughLinesP(img_edges, 1, np.pi / 180, threshold=threshold, minLineLength=minLineLen, maxLineGap=maxLineGap)
            return imgLines
        
        def extractLines_adaptativThreshold(self, img_rgb, color : Literal['R', 'G'], num_lines) :
            """Extracts lines from an RGB image with an adaptativ threshold and returns the main lines list wth the number of iteration of the threshold adaptation"""
            minLineLen = 25
            maxLineGap = 25

            threshold_step = 3
            init_threshold = 25
            threshold = init_threshold + threshold_step
            
            img_edges = self.extractEdges(img_rgb, color)
            imgLines = list()

            # On abaisse le seuil de détection d'une ligne tant qu'on n'en détecte pas suffisamment
            n = 0
            while len(imgLines) < num_lines :
                n += 1
                threshold -= threshold_step
                imgLines = cv2.HoughLinesP(img_edges, 1, np.pi/180, threshold=threshold, minLineLength=minLineLen, maxLineGap=maxLineGap)
                if threshold <= 0 :
                    break
                if imgLines is None :
                    imgLines = list()

            if imgLines is None :
                imgLines = list()

            mainLines = imgLines[:num_lines]
            return (mainLines, n)
        
        def getVanishingPoint(self, redLines, greenLines) :
            
            redPoint, greenPoint = True, True
            try :
                rline1 = redLines[0][0]
                rline2 = redLines[1][0]
                r_pt_y = 0.25*sum([rline1[1], rline1[3], rline2[1], rline2[3]])
                r_pt_x = max(rline1[0], rline1[2], rline2[0], rline2[2])
            except IndexError :
                redPoint = False
            
            try :
                gline1 = greenLines[0][0]
                gline2 = greenLines[1][0]
                g_pt_x = min(gline1[0], gline1[2], gline2[0], gline2[2])
                g_pt_y = 0.25*sum([gline1[1], gline1[3], gline2[1], gline2[3]])
            except IndexError :
                greenPoint = False

            criticalPoint = (0, 0)
            if redPoint and greenPoint :
                criticalPoint = (0.5*(r_pt_x + g_pt_x), 0.5*(r_pt_y + g_pt_y))
            else :
                if redPoint :
                    criticalPoint = (self.img_width, r_pt_y)
                elif greenPoint :
                    criticalPoint = (0, g_pt_y)

            return criticalPoint

        def embeddedCameraAnalysis(self) :
            """Process the embedded camera data"""
            img_rgb = self.getRGBImage()
            img_rgb_128 = cv2.resize(img_rgb, (self.img_width, self.img_height), interpolation=cv2.INTER_AREA)
            img_bgr_128 = cv2.cvtColor(img_rgb_128, cv2.COLOR_BGR2RGB)

            # Create masks for red and green colors
            red_bin_mask = self.extractColor(img_bgr_128, 'R')
            green_bin_mask = self.extractColor(img_bgr_128, 'G')

            # Lines extraction
            num_lines = 2
            redLines, _ = self.extractLines_adaptativThreshold(img_bgr_128, 'R', num_lines)
            greenLines, _ = self.extractLines_adaptativThreshold(img_bgr_128, 'G', num_lines)

            # Algebric absciss of the vanishing point
            vanishingPoint = self.getVanishingPoint(redLines, greenLines) # ([0, 128], [0, 128])
            vanishingPointAbs = vanishingPoint[0]

            # Vertcial red pixels histogram
            redHist = np.sum(red_bin_mask, axis=0)/255
            # Vertcial green pixels histogram
            greenHist = np.sum(green_bin_mask, axis=0)/255

            if self.robotWindowEnabled :
                # Irreversible gray transformation
                img_gray_128 = cv2.cvtColor(img_bgr_128, cv2.COLOR_BGR2GRAY)
                img_colorized_128 = cv2.cvtColor(img_gray_128, cv2.COLOR_GRAY2BGR)

                # Use the masks to apply red and green colors to the grayscale image
                img_colorized_128[red_bin_mask > 0, 0] = img_bgr_128[red_bin_mask > 0, 0]
                img_colorized_128[green_bin_mask > 0, 1] = img_bgr_128[green_bin_mask > 0, 1]

                # Plot red and green lines
                if redLines is not None :
                    for line in redLines:
                        x1, y1, x2, y2 = line[0]
                        cv2.line(img_colorized_128, (x1, y1), (x2, y2), (255, 0, 0), 2)
                if greenLines is not None :
                    for line in greenLines:
                        x1, y1, x2, y2 = line[0]
                        cv2.line(img_colorized_128, (x1, y1), (x2, y2), (0, 255, 0), 2)

                # Plot the vanishing point in sky blue
                cv2.circle(img_colorized_128, (int(vanishingPoint[0]), int(vanishingPoint[1])), 5, (255, 255, 0), -1)

                img_wwi = cv2.cvtColor(img_colorized_128, cv2.COLOR_BGR2RGB)
                retval, buffer = cv2.imencode('.jpeg', img_wwi)
                jpeg_as_text = base64.b64encode(buffer).decode()
                self.stateData['image'] = jpeg_as_text

            return redHist, greenHist, vanishingPointAbs
        
        def enableRobotWindow(self) :
            """Enable the robot window"""
            self.robotWindowEnabled = True

        def disableRobotWindow(self) :
            """Disable the robot window"""
            self.robotWindowEnabled = False

        def set_vitesse_m_s(self,vitesse_m_s) :
            speed = vitesse_m_s*3.6
            if speed > MAXSPEED :
                speed = MAXSPEED
            if speed < 0 :
                speed = 0
            super().setCruisingSpeed(speed)
        
        def set_direction_degre(self,angle_degre) :
            if angle_degre > MAXANGLE_DEGRE:
                angle_degre = MAXANGLE_DEGRE
            elif angle_degre < -MAXANGLE_DEGRE:
                angle_degre = -MAXANGLE_DEGRE   
            angle = -angle_degre * PI/180
            super().setSteeringAngle(angle)
        
        def crashed(self) :
            """Check if the car has crashed"""
            redColorPrct = 100 * self.stateData['redProportion']
            greenColorPrct = 100 * self.stateData['greenProportion']
            # If the proportion of red or green pixels is too high or too low, the car has crashed
            if redColorPrct > self.MONOCHROME_CRASH_PRCT_MAX or greenColorPrct > self.MONOCHROME_CRASH_PRCT_MAX :
                self.crashTimestepCounter += 1
            elif redColorPrct < self.MONOCHROME_CRASH_PRCT_MIN and greenColorPrct < self.MONOCHROME_CRASH_PRCT_MIN :
                self.crashTimestepCounter += 1
            else :
                self.crashTimestepCounter = 0

            if self.crashTimestepCounter > self.criticalCrashTimestep :
                self.crashTimestepCounter = 0
                self.crashCounter += 1
                return True
            else :
                # TODO : 
                if super().getTargetCruisingSpeed() < 0.01 and self.consigne_vitesse > 0.01 :
                    return True
                else :
                    return False
        
        def get_observation(self, init=False) :
            """Get the observation based on camera inputs"""
            current_redHist, current_greenHist, criticalPointAbs = self.embeddedCameraAnalysis()
            last_redHist = self.observation["redHist"]
            last_greenHist = self.observation["greenHist"]
            last_criticalPointAbs = self.observation["criticalPointAbs"][0]

            # Initialize histograms for observation
            redHist_norm = current_redHist / 128
            greenHist_norm = current_greenHist / 128
            last_redHist_norm = last_redHist / 128
            last_greenHist_norm = last_greenHist / 128

            if init :
                # Initialize critical point abscissa
                criticalPointAbs_norm = np.zeros(1)       # criticalPointAbs at center in [-1, 1]
                last_criticalPointAbs_norm =  np.zeros(1) # last_criticalPointAbs at center in [-1, 1]
                # Initialize speed and angle
                last_speed = np.zeros(1)                  # last_speed in [0, 1]
                last_angle = np.zeros(1) 			      # last_angle in [-1, 1]
            else :
                # Normalized critical point abscissa
                criticalPointAbs_norm = np.array([2*criticalPointAbs / 128 - 1]) # ([0, 128] -> [-1, 1])
                last_criticalPointAbs_norm = np.array([last_criticalPointAbs]) # last_criticalPointAbs in [-1, 1]
                # Normalized speed and angle
                last_speed = np.array([super().getCurrentSpeed() / VITESSE_MAX_M_S])
                last_angle = np.array([self.consigne_angle / MAXANGLE_DEGRE])

            observation = {
                "redHist" : redHist_norm,
                "greenHist" : greenHist_norm,
                "last_redHist" : last_redHist_norm,
                "last_greenHist" : last_greenHist_norm,
                "criticalPointAbs" : criticalPointAbs_norm,
                "last_criticalPointAbs" : last_criticalPointAbs_norm,
                "last_speed" : last_speed,
                "last_angle" : last_angle
            }

            self.stateData['speed'] = round(self.observation["last_speed"][0], 1)
            self.stateData['angle'] = round(self.observation["last_angle"][0], 1)
            self.stateData['redHist'] = redHist_norm.tolist()
            self.stateData['greenHist'] = greenHist_norm.tolist()
            self.stateData['redProportion'] = np.sum(redHist_norm) / 128
            self.stateData['greenProportion'] = np.sum(greenHist_norm) / 128

            # DYNAMIC MEASUREMENT
            # Absolute value of the normalized values
            self.speedAbsDerivative = abs(self.observation['last_speed'] - last_speed)
            self.steeringAbsDerivative = abs(self.observation['last_angle'] - last_angle)

            self.observation = observation
            return observation	
        
        def speedRewardFunction(self, speed_m_s) :
            """Non-linear reward function based on the speed"""

            V = speed_m_s * 3.6 # Convert speed to km/h
            Vmax = MAXSPEED     # Maximum speed in km/h

            Vnom_min = NOMSPEED_MIN # Minimum nominal speed in km/h
            Vnom_max = NOMSPEED_MAX   # Maximum nominal speed in km/h

            Rmin = -1000    # Minimum reward when V <= Vnom_min
            Rmin2 = -600   # Minimum reward when V >=  Vnom_max
            Rmax = 500     # Maximum reward when Vnom_min < V <= Vnom_max

            if V <= Vnom_min:
                # Growth power 4 for V <= Vnom
                R = Rmin + (Rmax - Rmin) * (V / Vnom_min) ** 3
            elif V <= Vnom_max:
                # Constant for Vnom_min < V <= Vnom_max
                R = Rmax
            else :
                # Quadratic decrease for V > Vnom
                R = Rmin2 + ((Vmax - V) / (Vmax - Vnom_max)) ** 5 * (Rmax - Rmin2)

            return R
        
        def get_reward(self, obs) :
            """Fonction de récompense basée sur les entrées de la caméra."""

            # COLOR IMBALANCE PENALTY
            # Color proportion in [0, 1]
            redProp = sum(obs['redHist'])/128   
            greenProp = sum(obs['greenHist'])/128    
            colorImbalanceFactor = 700
            colorImbalancePenalty = -(abs(redProp - greenProp)*colorImbalanceFactor)**1.3
            self.rewardsDict['colorImbalancePenalty'] = colorImbalancePenalty

            # SPEED REWARD
            current_speed = VITESSE_MAX_M_S * obs['last_speed'][0]  # Assumant que la vitesse est normalisée entre 0 et 1
            self.rewardsDict['speedReward'] = self.speedRewardFunction(current_speed)

            done = self.crashed()

            if done :
                self.crashCounter += 1
                self.rewardsDict['crashPenalty'] = -10000
            else :
                self.rewardsDict['crashPenalty'] = 0

            # Condition pour réinitialiser l'environnement après un certain nombre de pas, si nécessaire
            self.noResetCounter += 1
            if self.noResetCounter % RESET_STEP == 0 :
                done = True

            reward = 0.0
            for key in self.rewardsDict.keys() :
                if np.isscalar(self.rewardsDict[key]):
                    reward += float(self.rewardsDict[key])
                    print('{} = {}'.format(key, float(self.rewardsDict[key])))
                else:
                    reward += float(self.rewardsDict[key][0])
                    print('{} = {}'.format(key, float(self.rewardsDict[key][0])))
                
            reward = np.round(reward, 1)

            if self.robotWindowEnabled :
                self.stateData['rewardsMemory'].append(reward)
                if len(self.stateData['rewardsMemory']) > self.rewardsMemory :
                    self.stateData['rewardsMemory'].pop(0)
                self.stateData['rewardsDict'] = self.stateData['rewardsDict']

            return reward, done
        
        def reset(self, seed=None) :
            """Reset the car position and return an observation."""

            # Reset speed and steering angle
            self.consigne_vitesse = 0
            self.consigne_angle = 0
            self.set_vitesse_m_s(self.consigne_vitesse)
            self.set_direction_degre(self.consigne_angle)

            for i in range(20) :
                super().step()	

            self.noResetCounter = 0
            
            if(self.crashCounter != 0) :         
                # Wait for the car to stop
                while abs(super().getCurrentSpeed()) >= 0.001 :    		           	
                    print("Waiting TOTO for the car to stop...")
                    self.set_vitesse_m_s(0)
                    self.set_direction_degre(0)
                    super().setCruisingSpeed(0)
                    super().step()
        
                # Return an observation
                self.packet_number += 1
                # Send a message to the supervisor to reset the car
                self.emitter.send("CRASH n°{}".format(str(self.packet_number)))
                super().step()

                # Wait for the supervisor to reset the car
                while(self.receiver.getQueueLength() == 0) :        	
                    self.set_vitesse_m_s(self.consigne_vitesse)
                    super().step()

                data = self.receiver.getString()
                self.receiver.nextPacket()
                #print(data)
                    
                self.consigne_vitesse = 0
                self.consigne_angle = 0
                self.set_vitesse_m_s(self.consigne_vitesse )
                self.set_direction_degre(self.consigne_angle)
                #on fait quelques pas à l'arrêt pour stabiliser la voiture si besoin
                while abs(super().getTargetCruisingSpeed()) >= 0.001 :    		           	
                    #print("voiture pas arrêtée")
                    self.set_vitesse_m_s(0)
                    super().step()
                    
            info = {}        
            return self.get_observation(True), info
        
        # Step function
        def step(self, action) :

            """Perform a step in the environment."""
            current_speed = self.consigne_vitesse
            current_angle = self.consigne_angle

            # Extract actions from the dictionary
            speed_action = action[0]  # Assuming action space is defined with shape (1,)
            angle_action = action[1]  # Assuming action space is defined with shape (1,)

            incremental_speed_factor = 0.05
            incremental_angle_factor = 9.0
            
            self.consigne_vitesse = (current_speed + speed_action*incremental_speed_factor)
            self.consigne_angle = (current_angle + angle_action*incremental_angle_factor)
            
            # saturations
            if self.consigne_angle > MAXANGLE_DEGRE  :
                self.consigne_angle = MAXANGLE_DEGRE 
            elif self.consigne_angle < -MAXANGLE_DEGRE  :
                self.consigne_angle = -MAXANGLE_DEGRE 
            
            if self.consigne_vitesse > VITESSE_MAX_M_S :
                self.consigne_vitesse = VITESSE_MAX_M_S
            if self.consigne_vitesse < VITESSE_MIN_M_S :
                self.consigne_vitesse = VITESSE_MIN_M_S
                
            self.set_vitesse_m_s(self.consigne_vitesse)
            self.set_direction_degre(self.consigne_angle)
            super().step()

            if self.robotWindowEnabled :
                self.wwiSendText(json.dumps(self.stateData))
            
            obs = self.get_observation()
            reward, done = self.get_reward(obs)
            truncated = False
            info = {}

            return obs, reward, done, truncated, info

    def mainStepByStep(stepsPerLearning=10000, learningNumbers=10):
        print("LAUNCH LEARNING STEP BY STEP")
        env = WebotsGymEnvironment()
        env.enableRobotWindow()
        print("RLVision: environment built.")
        check_env(env)
        print("RLVision: environment checked.")

        for i in range(learningNumbers):
            # Model definition
            model = PPO(policy="MultiInputPolicy",
                        env=env,
                        learning_rate=5e-4,  # 5e-4
                        verbose=1,
                        device='cuda',
                        tensorboard_log='./PPO_ModelEvolution',)

            if i > 0 :
                model_file = f"./ModelEvolution/model_{i}.zip"
                print("LATTER MODEL LOADING : " + model_file)
                model = PPO.load(model_file)
                model.set_env(env)
            else :
                pass

            # Launch learning
            model.learn(total_timesteps=stepsPerLearning)

            # Save the model
            model.save(f"./ModelEvolution/model_{i+1}")

        print("Learning completed.")

    def mainTestNoTrain(model_name) :

        env = WebotsGymEnvironment()
        env.enableRobotWindow()

        print("RLVision : environnement built.")
        check_env(env)
        print("RLVision : environnement checked.")
        
        # Model definition
        model = PPO(policy="MultiInputPolicy",
            env=env, 
            learning_rate=8e-4, #5e-4
            verbose=1,
            device='cuda',
            tensorboard_log='./PPO_Tensorboard',)
                # Additional parameters
            # 
            # n_steps=2048,
            # batch_size=64, # Factor of n_steps
            # n_epochs=10, # Number of policy updates per iteration of n_steps
            # gamma=0.99,
            # gae_lambda=0.95,
            # clip_range=0.2,
            # vf_coef=1,
            # ent_coef=0.01)

        # Load learning data
        model_file = model_name
        print("Loading learning data from " + model_file)
        model = PPO.load(model_file)
        model.set_env(env)

        # Demo of the model
        obs, info = env.reset()
        print("Demo of the results.")
        c = 0
        while True :
            # Play the demo
            action, _ = model.predict(obs)
            obs, reward, done, _, info = env.step(action)
            c += reward
            print(reward)
            if done:
                obs, info = env.reset()
        print("Exiting.")
        print(c)

    if __name__ == '__main__' :
        mainStepByStep()
        #mainTestNoTrain("./ModelBASE_Speed/model_22.zip")

    ```


## Conclusion

Le simulateur Webots s'est avéré être un outil essentiel dans le développement de notre projet de voiture autonome Covpasy 2024. Il nous a permis d'entraîner notre réseau de neurones et d'évaluer ses performances dans un environnement de simulation réaliste. Nous recommandons vivement l'utilisation de Webots pour tout projet de développement de systèmes robotiques.
